# Miner
from miners.site_miner import SiteMiner
# Parser
from parsers.pageparser import PageParser
# Plugins
from plugins.authorfilter import AuthorFilter
from plugins.brokenlinkdata import BrokenLinkData
from plugins.datacleanup import DataCleanup
from plugins.dataexporter import DataExporter
from plugins.dataimporter import DataImporter
from plugins.datefilter import DateFilter
from plugins.linkfilter import LinkFilter
from plugins.sentimentanalyzer import SentimentAnalyzer
from plugins.sitemapper import SiteMapper
from plugins.textanalyzer import TextAnalyzer
from plugins.urllistbuilder import URLListBuilder

miner = SiteMiner()

########################################################################
# miner miner.settings
########################################################################

miner.settings['max_items'] = {}
miner.settings['max_items']['total'] = 10000
miner.settings['max_items']['internal'] = 10000
miner.settings['max_items']['external'] = 1
miner.settings['max_items']['file'] = 1
miner.settings['periodic_update_interval'] = 500
miner.settings['mine_internal'] = True
miner.settings['mine_external'] = False
miner.settings['mine_file'] = False
miner.settings['parser_debug_mode'] = False
miner.settings['headers'] = {'user-agent': 'QuoteMine Web Text Analyzer/0.0.1'}
miner.settings['kw'] = {'stream': True, 'timeout': 10}
miner.settings['domain'] = 'http://www.insert-news-site.com'
miner.settings['url_list'] = {'internal':['http://www.insert-news-site.com/'], 'external': [], 'file': []}
miner.settings['report_path'] = 'reports/cnn/'
miner.settings['time_between_requests'] = 2

########################################################################
# Plugin miner.settings
########################################################################

# Load plugins
text_analyzer = TextAnalyzer()
link_filter = LinkFilter()
date_filter = DateFilter()
page_parser = PageParser()
data_exporter = DataExporter()
data_importer = DataImporter()

#### Plugin settings for TextAnalyzer

text_analyzer.settings['min_length'] = 3
# Include the following words even if they are invalid
text_analyzer.settings['include'] = []
# Exclude the following words even if they are valid
text_analyzer.settings['exclude'] = ['the']
# Set up word and sentence parsers
text_analyzer.settings['parsers'] = ['parse_words', 'parse_tags'] # ,'parse_sentences'
text_analyzer.settings['parse_tags'] = ['title', 'h1', 'h2']
text_analyzer.settings['parser_report_types'] = ['total']
text_analyzer.settings['parsers_add_data_to_main'] = False
# Regex-based search. Email: \w+@\w+\.\w+ , Word with Space: word\swith\sspace will find 'word with space'
# Learn more at regexr.com
text_analyzer.settings['simple_page_search'] = [('Selfies', '[Ss]elfies?')]
text_analyzer.settings['simple_tag_search_tags'] = ['title', 'h1', 'h2']
text_analyzer.settings['simple_tag_search_terms'] = text_analyzer.settings['simple_page_search']
# Import/Export settings
text_analyzer.settings['import_path'] = miner.settings['report_path'] + 'textanalyzer_export.dat'
text_analyzer.settings['export_path'] = miner.settings['report_path'] + 'textanalyzer_export.dat'

#### Plugin settings for LinkFilter

link_filter.settings['require'] = []
link_filter.settings['exclude'] = ['image/gif', 'base64', '#', '?', 'xmlrpc', 'search', 'javascript:']
link_filter.settings['section_position'] = 3
link_filter.settings['report_methods'] = ['csv_output', 'd3_chart']
link_filter.settings['import_path'] = 'linkfilter_data.json'
link_filter.settings['export_path'] = 'linkfilter_data.json'

###### Plugin Settings for DataImporter

data_importer.settings['import_path'] = miner.settings['report_path'] + 'raw_export.dat'

###### Plugin Settings for DataExporter

data_exporter.settings['export_path'] = miner.settings['report_path'] + 'raw_export.dat'

#### Miner plugins - called for the entire mining process
miner.plugin_list['mine'] = {}

###### Miner pre-processing
miner.plugin_list['mine']['preprocess'] = []
###### Miner periodic updates
miner.plugin_list['mine']['periodic_update'] = [data_exporter, text_analyzer, link_filter]
###### Miner post-processing
miner.plugin_list['mine']['postprocess'] = []

#### URL Processor Plugins - called per URL minened
miner.plugin_list['url_processor'] = {'parse': [],'tag': [],'data': [],'postprocess': []}

###### URL Processor - parser
miner.plugin_list['url_processor']['parse'] = [page_parser]

###### URL Processor - handle_data callbacks
miner.plugin_list['url_processor']['data'] = [text_analyzer]

###### URL Processor - handle_tag callbacks
miner.plugin_list['url_processor']['tag'] = [link_filter, date_filter]

###### URL Processor - Process the response object post
miner.plugin_list['url_processor']['postprocess'] = []

#### Data Processor Plugins
miner.plugin_list['process_data'] = {}
miner.plugin_list['process_data']['process'] = []

#### Reporting Plugins - Generate Reports
miner.plugin_list['report'] = {}
miner.plugin_list['report']['preprocess'] = []
miner.plugin_list['report']['process'] = [text_analyzer, link_filter]
miner.plugin_list['report']['postprocess'] = []

#### Import/Export plugins
###### Import Plugins - Import Data
miner.plugin_list['import'] = {}
miner.plugin_list['import']['process'] = [data_importer, text_analyzer, link_filter]
miner.plugin_list['import']['postprocess'] = []

###### Export Plugins - Export Data
miner.plugin_list['export'] = {}
miner.plugin_list['export']['preprocess'] = []
miner.plugin_list['export']['process'] = [data_exporter, text_analyzer, link_filter]
miner.plugin_list['export']['postprocess'] = []


########################################################################
# Execute the miner!
########################################################################

# Importer Example - Comment "Run the mine" and uncomment this to pull data from an old report
#miner.import_data()

# Run the mine
miner.mine_site()

# Process data
miner.process_data()

# Generate reports from data
miner.generate_report()

# Export data for later use:
miner.export_data()
