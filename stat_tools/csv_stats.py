#imports
import os
import re
import csv
import numpy


""" 
Goals:

x Extract rows by category
- Perform statistics on rows/cols by category
  x Count
  x Sum
  x Average
  - Variance
  x St. Dev.
  - R^2
  x Mean/Median
  - ANOVA

- Create categories by interpreting data:
  - Numeric Ranges
  - Applying searches to columns

- Generate reports for each stat type
  x Count
  x Sum
  x Average
  - Variance
  x St. Dev.
  - R^2
  x Mean/Median
  - ANOVA

- Generate Combination reports/Merge Data sets
- Create extended stats options for economic stats, etc.

"""

########################################################################
### CSVData - Store CSV data and useful metadata
########################################################################

class CSVData:

  settings = {}
  settings['empty_cell'] = ''

  metadata = {}
  metadata['source_file'] = ''
  metadata['columns'] = {'import_data': 0}
  metadata['rows'] = {'import_data': 0}
  metadata['header_row'] = {'import_data': []}
  
  data = {'import_data': []}

  # Get dimensions of current data set, and set them in the metadata
  def get_dimensions(self, source = 'import_data'):
    width = 0
    length = len(self.data[source])
    for row in self.data[source]:
      if isinstance(row, list):
        if len(row) > width:
          width = len(row)
    self.metadata['columns'][source] = width
    self.metadata['rows'][source] = length
    return {'columns':width, 'rows':length} 

  # Process data in a given row, and add a new column
  def add_column(self, callback, source = 'import_data', **kwargs):
    i = 0
    kw = {}
    if kwargs:
      kw = kwargs
    if 'header' in kwargs:
      self.metadata['header_row'][source].append(kwargs['header'])
    for row in self.data[source]:
      new_col = callback(row, **kw)
      row = row + [new_col]
      self.data[source][i] = row
      i += 1
    

########################################################################
### CSVManager - Read and write CSV files, manage metadata
########################################################################

class CSVManager:
  settings = {}
  settings['read_file'] = 'none.csv'
  settings['write_file'] = 'dest.csv'
  settings['null_field'] = {'default': 0}
  settings['empty_row_handler'] = 'fill' #delete #none
  settings['values_cleanup_rules'] = {}
  settings['read_preprocess'] = []

  def generate_null_row(self, length):
    ret_row = [self.settings['null_field']['default']] * length
    for item in self.settings['null_field']:
      if isinstance(item, int) and item < len(ret_row):
        ret_row[item] = self.settings['null_field'][item]
    return ret_row

  def get_header_index(self, head_row, match_str):
    return head_row.index(match_str)

  def get_dimensions(self, f):
    width = 0
    length = 0
    with open(f, 'r') as fi:
      reader = csv.reader(fi, delimiter=',', quotechar='"')
      for row in reader:
        length += 1
        if len(row) > width:
          width = len(row)
    return {'columns':width, 'rows':length}

  # Determine if a row is empty
  def row_is_empty(self, row):
    for item in row:
      if item:
        return False
    return True

  # Empty row cleanup: modify or remove empty rows
  def empty_row_cleanup(self, f, **kwargs):
    #read_data
    data = []
    width = 0
    if self.settings['empty_row_handler'] == 'fill':
      dim = self.get_dimensions(f)
      width = dim['columns']
    with open(f, 'r') as fi:
      reader = csv.reader(fi, delimiter=',', quotechar='"')
      for row in reader:
        empt = self.row_is_empty(row)
        if empt and self.settings['empty_row_handler'] == 'delete':
          pass
        elif empt and self.settings['empty_row_handler'] == 'fill':
          data.append(self.generate_null_row(width))
        else:
          data.append(row)
    #write data
    if 'destination' in kwargs:
      dest = kwargs['destination']
    else:
      dest = f
    with open(dest, 'w') as fi:
      writer = csv.writer(fi, delimiter=',', quotechar='"')
      for row in data:
        writer.writerow(row)

  # Clean up row widths by making them uniform
  def row_width_cleanup(self, f, **kwargs):
    dim = self.get_dimensions(f)
    target_width = dim['columns']
    data = []
    #read_data
    with open(f, 'r') as fi:
      reader = csv.reader(fi, delimiter=',', quotechar='"')
      for row in reader:
        if len(row) == target_width:
          pass
        else:
          a = [self.settings['null_field']['default']] * (target_width - len(row))
          row = row + a
        data.append(row)
    #write data
    if 'destination' in kwargs:
      dest = kwargs['destination']
    else:
      dest = f
    with open(dest, 'w') as fi:
      writer = csv.writer(fi, delimiter=',', quotechar='"')
      for row in data:
        writer.writerow(row)

  # clean up values by running 'replace' commands on specified columns
  def row_values_cleanup(self, f, **kwargs):
    dim = self.get_dimensions(f)
    target_width = dim['columns']
    data = []
    #read data and modify as needed
    with open(f, 'r') as fi:
      reader = csv.reader(fi, delimiter=',', quotechar='"')
      for row in reader:
        new_row = []
        for item in row:
          op_list = []
          if row.index(item) in self.settings['values_cleanup_rules']:
            op_list = op_list + self.settings['values_cleanup_rules'][row.index(item)]
          if 'all' in self.settings['values_cleanup_rules']:
            op_list = op_list + self.settings['values_cleanup_rules']['all']
          for op in op_list:
            item = item.replace(op[0],op[1])
          new_row.append(item)
        data.append(new_row)
    #write data
    if 'destination' in kwargs:
      dest = kwargs['destination']
    else:
      dest = f
    with open(dest, 'w') as fi:
      writer = csv.writer(fi, delimiter=',', quotechar='"')
      for row in data:
        writer.writerow(row)
  ######################################################################
  #
  # Read a CSV File
  #
  # Keyword arguments
  #
  # preprocess: List containing 'empty_row_cleanup' or 'row_width_cleanup'
  # - Use this to invoke the respective functions for file cleanup before reading
  # - These will edit the existing file
  #
  # columns: List of integers inidicating column indeces.
  # - Example: [1, 5, 3] will build CSV data with the 1st column first, 
  #   5th column second, and 3rd column third.
  #
  # filters: List of tuples of the format (int, str, str)
  # - If a row fails to meet ALL criteria, it is not read
  # - The first value is the column index AFTER the column logic has been applied.
  # - The second value is the filter type: 
  #   -require: must contain a substring
  #   -exclude: must not contain a substring
  #   -re_match: must contain a match to a regular expression
  # - The third value is the string that goes with the above mentioned method
  #
  # has_headers: Boolean indicating the presence of a header row
  # - This will read the first row into metadata instead of regular data
  # - This effect is applied before filtering, as headers often would not
  #   not match filter criteria for data.
  def read_csv_file(self, f = settings['read_file'], **kwargs):
    if 'preprocess' in kwargs:
      if 'empty_row_cleanup' in kwargs['preprocess']:
        self.empty_row_cleanup(f)
      if 'row_width_cleanup' in kwargs['preprocess']:
        self.row_width_cleanup(f)
      if 'row_values_cleanup' in kwargs['preprocess']:
        self.row_values_cleanup(f)
    with open(f, 'r') as fi:
      reader = csv.reader(fi, delimiter=',', quotechar='"')
      data_obj = CSVData()
      # Collect settings
      cols = False
      filt = False
      has_headers = False
      first_row = True
      if 'columns' in kwargs:
        cols = kwargs['columns']
      if 'filters' in kwargs:
        filt = kwargs['filters']
      if 'has_headers' in kwargs:
        has_headers = kwargs['has_headers']
      # Does the CSV file have headers? Look for he settings first, 
      # then look for whether the columns argument has strings
      for i in cols:
        if isinstance(i, str):
          has_headers = True
      if not has_headers:
        first_row = False
      # Read in the data
      for row in reader:
        ret_row = []
        # If this is the the header row, record it in the data object's metadata
        if first_row and has_headers:
          # Build column list from given list of column headers
          new_cols = []
          for i in cols:
            new_cols.append(self.get_header_index(row, i))
          cols = new_cols
        # Build data columns if information was given.
        if cols:
          for i in cols:
            ret_row.append(row[i])
        else:
          ret_row = row
        # Finally, add metadata for the header row
        if first_row and has_headers:
          data_obj.metadata['header_row']['import_data'] = ret_row
          first_row = False
          ## Temporary: adds the head row to the data to help with writing
          ## Long term this needs to go into the writer
          data_obj.data['import_data'].append(ret_row)
        # Apply exclude/require filtering. Use 're_match' to invoke regex
        elif filt:
          add_data = True
          for fil in filt:
            # Require: Valid entries MUST have this substring
            if fil[1] == 'require':
              if fil[2] not in row[fil[0]]:
                add_data = False
            # Require in list: Valid entries MUST have ONE OF the listed substrings
            elif fil[1] == 'require_list':
              add_data = False
              for item in fil[2]:
                if item in row[fil[0]]:
                  add_data = True
            # Exclude: Valid entries MUST NOT have this substring
            elif fil[1] == 'exclude':
              if fil[2] in row[fil[0]]:
                add_data = False
            # Regular expression match: Valid entries MUST match the regex statement
            elif fil[1] == 're_match':
              matches = re.findall(fil[2], row[fil[0]])
              if not matches:
                add_data = False
            # Regular expression match in list: Valid entries MUST match ONE OF the listed regex statements
            elif fil[1] == 're_match_list':
              add_data = False
              for item in fil[2]:
                matches = re.findall(item, row[fil[0]])
                if matches:
                  add_data = True
            # Greater Than, etc.: Valid entries MUST pass the given numeric test
            elif fil[1] in ['greater_than', 'less_than', 'greater_equal', 'less_equal']:
              val = False
              try:
                val = float(row[fil[0]])
              except:
                add_data = False
              if val or val == 0 or val == 0.0:
                if fil[1] == 'greater_than' and not (val > fil[2]):
                  add_data = False
                elif fil[1] == 'greater_equal' and not (val >= fil[2]):
                  add_data = False
                elif fil[1] == 'less_than' and not (val < fil[2]):
                  add_data = False
                elif fil[1] == 'less_eqaul' and not (val <= fil[2]):
                  add_data = False
          if add_data:
            data_obj.data['import_data'].append(ret_row)
        else:
          data_obj.data['import_data'].append(ret_row)
      # Generate additional metadata
      dim = self.get_dimensions(f)
      data_obj.metadata['source_file'] = f
      data_obj.metadata['columns']['import_data'] = dim['columns']
      data_obj.metadata['rows']['import_data'] = dim['rows']
      # Now that we have built the data object, return it for further use
      return data_obj

  ######################################################################
  #
  # Write a CSV File
  #
  # Keyword arguments
  #
  # data_name: String containing a dictionary index
  # - Pulls the appropriate data from the data_obj.data dictionary
  # - To get all data, write a for loop, call this function for each 
  # index, and provide a filename for each piece of data
  def write_csv_file(self, data_obj, dest = 'none', **kwargs):
    if dest == 'none':
      dest = self.settings['write_file']
    data_name = ''
    if 'data_name' in kwargs:
      data_name = kwargs['data_name']
    with open(dest, 'w') as fi:
      data = data_obj.data[data_name]
      writer = csv.writer(fi, delimiter=',', quotechar='"')
      if data_name in data_obj.metadata['header_row'] and data_obj.metadata['header_row'][data_name]:
        writer.writerow(data_obj.metadata['header_row'][data_name])
      for row in data:
        if isinstance(row, list):
          writer.writerow(row)
        elif isinstance(row, dict):
          new_row = []
          for section in row:
            if isinstance(row[section], list):
              for item in row[section]:
                new_row.append(item)
            else:
              new_row.append(row[section])
          writer.writerow(new_row)
        else:
          writer.writerow([row])
    
    

########################################################################
### CSVStats - Calculate statistics from CSV Data
########################################################################

class CSVStats:
  data_obj = ''
  
  def __init__(self, data_obj = ''):
    self.data_obj = data_obj
  
  # Generate a categorized list: creates a dict with category info + data
  # single_category: forces a category of 'all' to very piece of data
  def generate_categories(self, cols = [0], source = 'import_data', destination = 'categorized_import_data', **kwargs):
    single = False
    if 'single_category' in kwargs:
      single = kwargs['single_category']
    self.data_obj.data[destination] = []
    for row in self.data_obj.data[source]:
      if not single:
        row_cat = []
        for col in cols:
          row_cat.append(row[col])
        if len(row_cat) == 1:
          row_cat = row_cat[0]
        else:
          row_cat = tuple(row_cat)
        self.data_obj.data[destination].append({'category': row_cat, 'data': row})
      else:
        self.data_obj.data[destination].append({'category': 'all', 'data': row})
  
  # Group rows into categories. Assumes a categorized list has been created.
  def group_into_categories(self, source = 'categorized_import_data', destination = 'category_grouped_data'):
    if not self.data_obj.data[source]:
      self.generate_categories([0], 'import_data', source)
    self.data_obj.data[destination] = {}
    for row in self.data_obj.data[source]:
      cat = row['category']
      if cat in self.data_obj.data[destination]:
        self.data_obj.data[destination][cat].append(row['data'])
      else:
        self.data_obj.data[destination][cat] = [row['data']]
  
  # Group rows into categories. Assumes a categorized list has been created, 
  # with categories containing more than one piece of data.
  def group_into_categories_2d(self, source = 'categorized_import_data', destination = 'category_2d_grouped_data'):
    if not self.data_obj.data[source]:
      self.generate_categories([0, 1], 'import_data', source)
    self.data_obj.data[destination] = {}
    rows = []
    cols = []
    for row in self.data_obj.data[source]:
      cat = row['category']
      if cat[0] not in self.data_obj.data[destination]:
        self.data_obj.data[destination][cat[0]] = {cat[1]: [row['data']]}
      elif cat[1] not in self.data_obj.data[destination][cat[0]]:
        self.data_obj.data[destination][cat[0]][cat[1]] = [row['data']]
      else:
        self.data_obj.data[destination][cat[0]][cat[1]].append(row['data'])

      if cat[0] not in rows:
        rows.append(cat[0])
      if cat[1] not in cols:
        cols.append(cat[1])
      self.data_obj.metadata['rows'][destination] = rows
      self.data_obj.metadata['columns'][destination] = cols
        

  # Generate count report for all categories or in-category data.
  # Assumes group_into_categories has been run.
  # This is separate from numeric_report as it may singularly be used with text-based counts
  def cat_count_report(self, source = 'category_grouped_data', destination = 'count_report'):
    data = self.data_obj.data[source]
    self.data_obj.data[destination] = []
    for cat in data:
      ret_row = []
      count = len(data[cat])
      if isinstance(cat, tuple):
        for item in cat:
          ret_row.append(item)
        ret_row.append(count)
      else:
        ret_row = [cat, count]
      self.data_obj.data[destination].append(ret_row)

  # Create a two-dimensional count report from categorized data. 
  # Assumes that at least two pieces of data were used to create categories.
  def cat_count_report_2d(self, source = 'category_2d_grouped_data', destination = 'count_2d_report'):
    # First, we need the full list of categories from each dimension
    rows = self.data_obj.metadata['rows'][source]
    cols = self.data_obj.metadata['columns'][source]
    self.data_obj.metadata['header_row'][destination] = [''] + cols
    data = self.data_obj.data[source]
    self.data_obj.data[destination] = []
    for row in rows:
      ret_row = [row]
      for col in cols:
        if col in data[row]:
          ret_row.append(len(data[row][col]))
        else:
          ret_row.append(0)
      self.data_obj.data[destination].append(ret_row)

  # Takes a data source and computes a multi-axis values column. Only keeps numeric data
  def cat_values(self, cols = [0], source = 'category_grouped_data'):
    data = self.data_obj.data[source]
    cat_vals = {}
    for cat in data:
      cat_vals[cat] = []
      for row in data[cat]:
        ret_row = []
        for col in cols:
          if row[col]:
            try:
              ret_row.append(float(row[col]))
            except:
              pass
          else:
            ret_row.append(0)
        cat_vals[cat].append(ret_row)
    return cat_vals

  # Standard statistics - these are items that can be called directly 
  # from numpy on a cat_values column
  def cat_standard_stats(self, vals = {'none':[0]}, ops = ['count', 'sum', 'low', 'high', 'median', 'mean', 'stdev'], destination = 'category_standard_stats', **kwargs):
    #add function-specific keyword arguments
    std_kw = {'dtype':numpy.float64, 'axis':0}
    if 'stdev_params' in kwargs:
      for item in kwargs['stdev_params']:
        std_kw[item] = kwargs['stdev_params'][item]
    self.data_obj.data[destination] = []
    
    for cat in vals:
      # Build the category labels
      ret_row = []
      if isinstance(cat, tuple):
          for item in cat:
            ret_row.append(item)
      else:
        ret_row = [cat]
      # Count
      if 'count' in ops:
        count = len(vals[cat])
        ret_row.append(count)
      # Sum
      if 'sum' in ops:
        s = numpy.sum(vals[cat], axis=0)
        ret_row = ret_row + list(s)
      if 'low' in ops:
        l = numpy.percentile(vals[cat], 0, axis=0)
        ret_row = ret_row + list(l)
      if 'high' in ops:
        h = numpy.percentile(vals[cat], 100, axis=0)
        ret_row = ret_row + list(h)
      if 'median' in ops:
        m = numpy.percentile(vals[cat], 50, axis=0)
        ret_row = ret_row + list(m)
      if 'mean' in ops:
        mn = numpy.mean(vals[cat], axis=0, dtype=numpy.float64)
        ret_row = ret_row + list(mn)
      if 'stdev' in ops:
        st = numpy.std(vals[cat], **std_kw)
        ret_row = ret_row + list(st)

      # Finally, add the completed row
      self.data_obj.data[destination].append(ret_row)
  
  # Include this in the extended/economic stats later
  def cat_gini_report(self, vals = {'none':[0]}, destination = 'category_gini', **kwargs):
    self.data_obj.data[destination] = []
    for cat in vals:
      # Build the category labels
      ret_row = []
      if isinstance(cat, tuple):
          for item in cat:
            ret_row.append(item)
      else:
        ret_row = [cat]
      # Calculate sum/length, we will need these in a moment
      cat_length = len(vals[cat])
      cat_sum = numpy.sum(vals[cat], axis=0, dtype=numpy.float64)
      cat_sum = float(cat_sum)
      # Go row by row to generate 
      current_sum = 0
      area_b = 0
      sorted_list = sorted(vals[cat])
      for row in sorted_list:
        current_sum += row[0]
        area_b += current_sum - row[0] / 2
      ideal_b = current_sum * cat_length / 2
      gini_coeff = (ideal_b - area_b) / ideal_b
      ret_row = ret_row + [cat_length, cat_sum, gini_coeff]
      # Finally, add the completed row
      self.data_obj.data[destination].append(ret_row)
