#imports
import os
import re
import csv
import ast

########################################################################
### CSVDataCallbacks - Common CSVData callbacks
#
# Contains several common callbacks used when working with CSVData. All
# callbacks must have the form func(self, row, **kwargs) to be 
# compatible.
#
########################################################################

class CSVDataCallbacks:
  
  # Get a year from a text date. Takes in the kwarg 'year range' which
  # is an integer list of years.
  def year_from_date(self, row, **kwargs):
    if 'year_range' in kwargs:
      year_range = kwargs['year_range']
    else:
      year_range = range(1950, 2050)
    for year in year_range:
      for item in row:
        if str(year) in item:
          return str(year)
  
  def list_first_item(self, row, **kwargs):
    if 'list_index' in kwargs:
      ind = int(kwargs['list_index'])
      try:
      # Assumes stringified list
        if '[' in row[ind]:
          lst = ast.literal_eval(row[ind])
          if lst:
            return lst[0]
          else:
            return 'none'
        else:
          return row[ind]
      except:
        return 'error'
