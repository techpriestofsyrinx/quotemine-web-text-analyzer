#imports
import os
import re
import csv
import csv_stats
import numpy

#Instantiate the CSV Manager
testread = csv_stats.CSVManager()

testread.settings = {}
testread.settings['read_file'] = 'cnn_data.csv'
testread.settings['write_file'] = 'csv_processor_result.csv'
testread.settings['data_object'] = csv_stats.CSVData()
testread.settings['null_field'] = {'default': []}
#testread.settings['values_cleanup_rules'] = {'all': [('$', ''), (',', '')]}
testread.settings['empty_row_handler'] = 'delete'

# Set up RegEx searches
regex_search_list = [ \
('Donald Trump', "Trump(?=[\W\s])|Trump$|Trump's"), \
('Ted Cruz', "Cruz(?=[\W\s])|Cruz$|Cruz's"), \
('Rand Paul', "Rand(?=[\W\s])(?=$)|Rand\sPaul|Rand's|Paul's|Paul(?!.Ryan)(?=[\W\s])|Paul$"), \
('Marco Rubio', "Rubio(?=[\W\s])|Rubio$|Rubio's"), \
('Lindsey Graham', "Graham(?=[\W\s])|Graham$|Graham's"), \
('Jeb Bush', "Jeb(?=[\W\s])|Jeb$|Jeb's|(?<!George.)(?<!W.)(?<!W..)Bush(?=[\W\s])|(?<!George.)(?<!W.)(?<!W..)Bush$|(?<!George.)(?<!W.)(?<!W..)Bush's"), \
('John Kasich', "Kasich(?=[\W\s])|Kasich$|Kasich's"), \
('Ben Carson', "Carson(?=[\W\s])|Carson$|Carson's"), \
('Carly Fiorina', "Fiorina(?=[\W\s])|Fiorina$|Fiorina's"), \
('Chris Christie', "Christie(?=[\W\s])|Christie$|Christie's"), \
('Mike Huckabee', "Huckabee(?=[\W\s])|Huckabee$|Huckabee's"), \
#~ ('Hillary Clinton', "Hillary(?=[\W\s])|Hillary$|Hillary's|(?<!Bill\s)(?<!Chelsea\s)Clinton(?=[\W\s])|(?<!Bill\s)(?<!Chelsea\s)Clinton$|(?<!Bill\s)(?<!Chelsea\s)Clinton's"), \
#~ ('Bernie Sanders', "Bernie(?=[\W\s])|Bernie$|Bernie's|Bern(?=[\W\s])(?=$)|Bern$|Bern's|Sanders(?=[\W\s])|Sanders$|Sanders's"), \
#~ ("Martin O'Malley", "O'Malley(?=[\W\s])|O'Malley$|O'Malley"), \
]

regex_statements = []
for tup in regex_search_list:
  regex_statements.append(tup[1])

# Read CSV data into a CSVData object.
f=[(9, 're_match_list', regex_statements), (1,'require', '2015')]
data = testread.read_csv_file(testread.settings['read_file'], filters=f)

# Callback: Assign categories based on RegEx matches
def call(row):
  for tup in regex_search_list:
    matches = re.findall(tup[1], row[9])
    if matches:
      return tup[0]

def call2(row):
  for year in ['2015', '2014', '2013', '2012', '2011', '2010']:
    if year in row[1]:
      return year

data.add_column(call)
data.add_column(call2)

# Get category data
stats = csv_stats.CSVStats(data)
stats.generate_categories([1, 11])
stats.group_into_categories_2d()
stats.cat_count_report_2d()
# Used with numeric data
#~ vals = stats.cat_values([7])
#~ stats.cat_standard_stats(vals, ['count', 'sum', 'low', 'high', 'median', 'mean', 'stdev'])
#~ stats.cat_gini_report(vals)

# Write our results into CSV files
for data_type in data.data:
  testread.write_csv_file(data, 'csv_test_' + data_type + '_report.csv', data_name=data_type)
