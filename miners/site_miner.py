import re
import os
import sys
import time
import csv
import requests
from os import mkdir

class Generic():
  pass

## Site Miner: carries out the actual work of reading each page

class SiteMiner():
  ######################################################################
  # Data and Constants
  ######################################################################
  app_object = False
  mine_data = {}

  url_count = {}
  url_count['total'] = 0
  url_count['internal'] = 0
  url_count['external'] = 0
  url_count['file'] = 0
  
  current_id = 0
  
  # The current revision. Default is a placeholder, but you can add the 
  # current time and date by using: time.strftime('%H:$M:%S-%d-%m-%Y',localtime())
  revision_id = 'revision'

  # If the scanner hits more than the max permitted scan errors in a row,
  # turn on debug mode to see the cause of the problem.
  scan_error_count = 0

  ######################################################################
  # settings and Plugin Settings
  ######################################################################
  settings = {}
  # Mining settings
  settings['max_items'] = {'total':1,'internal':1,'external':1,'file':1}
  settings['mine_internal'] = True
  settings['mine_file'] = False
  settings['mine_external'] = False
  settings['periodic_update_interval'] = 10
  settings['scan_error_max'] = 10
  
  # URL Processor settings
  settings['parser_debug_mode'] = False
  settings['headers'] = {'user-agent': 'QuoteMine Web Text Analyzer/0.0.1'}
  settings['kw'] = {'stream': True, 'timeout': 10}
  settings['domain'] = 'http://www.example.com'
  settings['url_list'] = {'internal':['http://www.example.com'], 'external': [], 'file': []}
  settings['time_between_requests'] = 3
  
  # Report settings
  settings['report_path'] = 'reports/'

  plugin_list = {}
  ## Miner plugins - called for the entire mining process
  plugin_list['mine'] = {'preprocess': [], 'periodic_update': [], 'postprocess': []}

  ## URL Processor Plugins - called per URL scanned
  plugin_list['url_processor'] = {'parse': [], 'tag': [], 'data': [], 'postprocess': []}

  ## Data Processor Plugins - Process Data
  plugin_list['process_data'] = {'process': []}
  
  ## Reporting Plugins - Generate Reports
  plugin_list['report'] = {'preprocess': [], 'process': [], 'postprocess': []}
  
  ## Import Plugins - Import Data
  plugin_list['import'] = {'process': [], 'postprocess': []}
  
  ## Export Plugins - Export Data
  plugin_list['export'] = {'preprocess': [], 'process': [], 'postprocess': []}

  ######################################################################
  # Helper functions
  ######################################################################
  
  # Internal print function that covers certain cases in loops. 'print()' doesn't work
  def pprint(self, text):
    if self.app_object:
      self.app_object.active_message = str(text)
    else:
      sys.stdout.write(str(text) + '\n')
      sys.stdout.flush()

  def get_url_id(self, url):
    if url in self.mine_data and 'id' in self.mine_data[url]:
      return int(self.mine_data[url]['id'])
    else:
      return int(self.current_id)

  def assign_url_id(self, url):
    if url in self.mine_data and 'id' in self.mine_data[url]:
      return int(self.mine_data[url]['id'])
    else:
      self.current_id += 1
      return int(self.current_id)
  
  def assign_url_revision_id(self):
    return self.revision_id

  # Create an empty data set
  def add_empty_url_data(self, url):
    if url not in self.mine_data:
      self.mine_data[url] = {}
      self.mine_data[url][self.revision_id] = {}
    elif self.revision_id not in self.mine_data[url]:
      self.mine_data[url][self.revision_id] = {}
  
  # Programmatic run - Run standard Quotemine functions without having
  # to call each function separately
  def programmatic_run(self, **kwargs):
    # If a reset is required later, hold the initial URL list here
    url_reset = self.settings['url_list']
    
    if 'load' in kwargs and kwargs['load']:
      self.import_data()
    if 'mine' in kwargs and kwargs['mine']:
      self.mine_site()
    if 'process' in kwargs and kwargs['process']:
      self.process_data()
    if 'report' in kwargs and kwargs['report']:
      self.generate_report()
    if 'export' in kwargs and kwargs['export']:
      self.export_data()
    if 'url_reset' in kwargs and kwargs['url_reset']:
      self.url_count['total'] = 0
      self.url_count['internal'] = 0
      self.settings['url_list'] = url_reset
    if 'mine_reset' in kwargs and kwargs['mine_reset']:
      self.url_count['total'] = 0
      self.url_count['internal'] = 0
      self.settings['url_list'] = url_reset
      self.mine_data = {}

  ######################################################################
  # URL Processors
  ######################################################################
  def process_url_full(self, url):
    kw = self.settings['kw']
    kw['headers'] = self.settings['headers']
    s = time.time()
    resp = requests.get(url, **kw)
    e = time.time()
    if url != resp.url:
      self.mine_data[url][self.revision_id]['redirect_url'] = resp.url
      resp.init_url = url
    else:
      if not self.revision_id in self.mine_data[url]:
        self.mine_data[url][self.revision_id] = {}
      self.mine_data[url][self.revision_id]['redirect_url'] = 'none'
      resp.init_url = url
    self.mine_data[url][self.revision_id]['duration'] = e - s
    self.mine_data[url][self.revision_id]['id'] = self.mine_data[url]['id']
    # Pause for the set sleep time. This should always be above 1 second, prefereably 3-5
    time.sleep(self.settings['time_between_requests'])
    self.mine_data[url][self.revision_id]['status'] = resp.status_code
    for plugin in self.plugin_list['url_processor']['parse']:
      kw = {}
      kw['tag_parser_list'] = []
      for p in self.plugin_list['url_processor']['tag']:
        kw['tag_parser_list'].append(p.url_processor_tag)
      kw['data_parser_list'] = [] 
      for p in self.plugin_list['url_processor']['data']:
        kw['data_parser_list'].append(p.url_processor_data)
      kw['miner_obj'] = self
      kw['active_url'] = url
      plugin.url_processor_parse(resp.text, **kw)

    # Run post processing with entire response object
    for plugin in self.plugin_list['url_processor']['postprocess']:
      plugin.url_processor_postprocess(resp, miner_obj=self)
    

  # Standard URL Processor - Attaches to Page Parser and contains callbacks for plugins
  def process_url(self, url):
    if not url in self.mine_data:
      self.add_empty_url_data(url)
    if 'id' not in self.mine_data[url]:
      self.mine_data[url]['id'] = self.assign_url_id(url)

    rev = self.assign_url_revision_id()
    if rev not in self.mine_data[url]:
      self.mine_data[url][rev] = {'id': self.mine_data[url]['id']}

    if self.settings['parser_debug_mode']:
      self.process_url_full(url)
    else:
      # If you are getting no found URLs or tons of scan errors, 
      # comment out the try/except statements so that only the 'try' code runs
      try:
        self.process_url_full(url)
        self.scan_error_count = 0

      # If something blows up for this URL, return a scan error in the url data
      except:
        self.pprint('Hit a scan error!')
        if not self.revision_id in self.mine_data[url]:
          self.mine_data[url][self.revision_id] = {}
        self.mine_data[url][self.revision_id]['duration'] = 0
        self.mine_data[url][self.revision_id]['status'] = 'Scan Error'
        self.mine_data[url][self.revision_id]['id'] = self.mine_data[url]['id']
        self.scan_error_count += 1
      
      # If we reach the specified threshold of repeating scan errors, turn on debug mode
      if self.scan_error_count and self.scan_error_count > self.settings['scan_error_max']:
        self.settings['parser_debug_mode'] = True
      

  # Minimal URL Processor - Assumes one is only checking for status code + load time
  def process_url_minimal(self, url):
    if not url in self.mine_data:
      self.add_empty_url_data(url)
    if 'id' not in self.mine_data[url]:
      self.mine_data[url]['id'] = self.assign_url_id(url)

    rev = self.assign_url_revision_id()
    if rev not in self.mine_data[url]:
      self.mine_data[url][rev] = {'id': self.mine_data[url]['id']}
    try:
      kw = self.settings['kw']
      kw['headers'] = self.settings['headers']
      s = time.time()
      resp = requests.get(url, **kw)
      e = time.time()
      self.mine_data[url][self.revision_id]['duration'] = e - s
      self.mine_data[url][self.revision_id]['status'] = resp.status_code
      self.mine_data[url][self.revision_id]['id'] = self.mine_data[url]['id']
      self.scan_error_count = 0
    
    except:
      self.mine_data[url][self.revision_id]['duration'] = 0
      self.mine_data[url][self.revision_id]['status'] = 'Scan Error'
      self.mine_data[url][self.revision_id]['id'] = self.mine_data[url]['id']
      self.scan_error_count += 1
    
    if self.scan_error_count and self.scan_error_count > self.settings['scan_error_max']:
      self.settings['parser_debug_mode'] = True

  ######################################################################
  # Site Miner
  ######################################################################

  # Mine site - Run through the url_list
  def mine_site(self):
    ## Pre-processing for scans; usually for data cleanup/list prep
    starting_id = 0
    for plugin in self.plugin_list['mine']['preprocess']:
      plugin.mine_preprocess(miner_obj=self)
    ## Pre-processing : calculate a starting ID
    if self.mine_data:
      for url in self.mine_data:
        if 'id' in self.mine_data[url] and self.mine_data[url]['id'] > starting_id:
          starting_id = self.mine_data[url]['id']
    self.url_count['total'] = starting_id
    self.settings['max_items']['total'] += starting_id

    if self.settings['mine_internal']:
      while self.settings['url_list']['internal'] and self.url_count['total'] < self.settings['max_items']['total'] and self.url_count['internal'] < self.settings['max_items']['internal']:
        url = self.settings['url_list']['internal'].pop(0)
        if self.url_count['total'] and self.url_count['total'] % self.settings['periodic_update_interval'] == 0:
          self.pprint('Carrying out periodic update...')
          for plugin in self.plugin_list['mine']['periodic_update']:
            plugin.mine_periodic_update(miner_obj=self)
        # Determine if we are scanning a new URL or re-scanning, and show an appropriate message
        current_id = self.get_url_id(url)
        if current_id == self.url_count['total']:
          self.pprint('Scanning #' + str(self.url_count['total'] + 1) + ': ' + url)
        else:
          self.pprint('Re-scanning #' + str(self.get_url_id(url) + 1) + ': ' + url)
        # Increase the URL count, and process the URL
        self.url_count['total'] += 1
        self.url_count['internal'] += 1
        self.process_url(url)
        self.mine_data[url][self.revision_id]['type'] = 'internal'
      if self.url_count['internal'] >= self.settings['max_items']['internal']:
        self.pprint('Reached the Section Maximum URL count of ' + str(self.settings['max_items']['internal']) + ' URLs for the "internal" category.')
      elif self.url_count['total'] >= self.settings['max_items']['total']:
        self.pprint('Reached the Maximum URL count of ' + str(self.settings['max_items']['total'] - starting_id) + ' URLs.')
      else:
        self.pprint('URL List is empty for Internal Category.')

    if self.settings['mine_external']:
      for url in self.settings['url_list']['external']:
        if self.url_count['total'] and self.url_count['total'] % self.settings['periodic_update_interval'] == 0:
          self.pprint('Carrying out periodic update...')
          for plugin in self.plugin_list['mine']['periodic_update']:
            plugin.mine_periodic_update(miner_obj=self)
        if self.url_count['total'] < self.settings['max_items']['total'] and self.url_count['external'] < self.settings['max_items']['external']:
          # Determine if we are scanning a new URL or re-scanning, and show an appropriate message
          current_id = self.get_url_id(url)
          if current_id == self.url_count['total']:
            self.pprint('Scanning #' + str(self.url_count['total'] + 1) + ': ' + url)
          else:
            self.pprint('Re-scanning #' + str(self.get_url_id(url) + 1) + ': ' + url)
          # Increase the URL count, and process the URL
          self.url_count['total'] += 1
          self.url_count['external'] += 1
          self.process_url_minimal(url)
          self.mine_data[url][self.revision_id]['type'] = 'external'
        elif self.url_count['external'] >= self.settings['max_items']['external']:
          self.pprint('Reached the Section Maximum URL count of ' + str(self.settings['max_items']['external']) + ' URLs for the "external" category.')
          break
        elif self.url_count['total'] >= self.settings['max_items']['total']:
          self.pprint('Reached the Maximum URL count of ' + str(self.settings['max_items']['total'] - starting_id) + ' URLs.')
        else:
          self.pprint('URL List is empty for External Category.')

    if self.settings['mine_file']:
      for url in self.settings['url_list']['file']:
        if self.url_count['total'] and self.url_count['total'] % self.settings['periodic_update_interval'] == 0:
          self.pprint('Carrying out periodic update...')
          for plugin in self.plugin_list['mine']['periodic_update']:
            plugin.mine_periodic_update(miner_obj=self)
        if self.url_count['total'] < self.settings['max_items']['total'] and self.url_count['file'] < self.settings['max_items']['file']:
          # Determine if we are scanning a new URL or re-scanning, and show an appropriate message
          current_id = self.get_url_id(url)
          if current_id == self.url_count['total']:
            self.pprint('Scanning #' + str(self.url_count['total'] + 1) + ': ' + url)
          else:
            self.pprint('Re-scanning #' + str(self.get_url_id(url) + 1) + ': ' + url)
          # Increase the URL count, and process the URL
          self.url_count['total'] += 1
          self.url_count['file'] += 1
          self.process_url_minimal(url)
          self.mine_data[url][self.revision_id]['type'] = 'file'
        elif self.url_count['file'] >= self.settings['max_items']['file']:
          self.pprint('Reached the Section Maximum URL count of ' + str(self.settings['max_items']['file']) + ' URLs for the "file" category.')
          break
        elif self.url_count['total'] >= self.settings['max_items']['total']:
          self.pprint('Reached the Maximum URL count of ' + str(self.settings['max_items']['total'] - starting_id) + ' URLs.')
        else:
          self.pprint('URL List is empty for File Category.')

    ## Post processing for scans; usually for data cleanup and for calculating statistics
    for plugin in self.plugin_list['mine']['postprocess']:
      plugin.mine_postprocess(miner_obj=self)

  ######################################################################
  # Data Handling (Import/Export/Process/Report)
  ######################################################################

  # Load a backup file: Use this to pull and work with old data
  def import_data(self):
    try:
      mkdir(self.settings['report_path'])
    except:
      pass
    ## Import data
    for plugin in self.plugin_list['import']['process']:
      plugin.import_data_process(miner_obj=self)

    ## Post-process imported data; usually for preparation for other operations
    for plugin in self.plugin_list['import']['postprocess']:
      plugin.import_data_postprocess(miner_obj=self)

  # Export data in whatever format is appropriate
  def export_data(self):
    try:
      mkdir(self.settings['report_path'])
    except:
      pass
    ## Pre-process data; usually for performing statistics to be used in reports
    for plugin in self.plugin_list['export']['preprocess']:
      plugin.export_data_preprocess(miner_obj=self)

    ## Export data
    for plugin in self.plugin_list['export']['process']:
      plugin.export_data_process(miner_obj=self)

    ## Post-process data; usually for preparation for other operations
    for plugin in self.plugin_list['export']['postprocess']:
      plugin.export_data_postprocess(miner_obj=self)

  # Process Data: Generic function to work on gathered data without an 
  # explicit need to generate reports or export data.
  def process_data(self):
    ## Process Data
    for plugin in self.plugin_list['process_data']['process']:
      plugin.process_data(miner_obj=self)

  # Generate Report: Call all plugin report data
  def generate_report(self):
    try:
      mkdir(self.settings['report_path'])
    except:
      pass
    ## Pre-process data; usually for performing statistics to be used in reports
    for plugin in self.plugin_list['report']['preprocess']:
      plugin.report_data_preprocess(miner_obj=self)

    ## Generate reports
    for plugin in self.plugin_list['report']['process']:
      plugin.report_data_process(miner_obj=self)

    ## Post-process data; usually for preparation for other operations
    for plugin in self.plugin_list['report']['postprocess']:
      plugin.report_data_postprocess(miner_obj=self)

  ######################################################################
  # Repeat Mining
  ######################################################################

  # Repeat Mine function - permits continuous monitoring
  #
  # kwargs: argument, with descriptions and attional sub-arguments
  #
  # repeat_type: 'repeat', 'interval', or 'multi_folder_interval'
  # - repeat: repeat a certain number of times (defaults to 1)
  # --- repeats: Number of times to repeat
  # - interval: repeat for a certain time interval
  # --- runtime: Total time to run repeats (defaults to 3600)
  # --- interval: Time per run interval. If the run is faster then the 
  #               interval, the program will wait. If it takes longer, 
  #               the next run will start immediately (defaults to 100)
  # --- wait: Number of seconds to wait before checking for the next 
  #           interval (defaults to 1)
  # - multi_folder_interval: Interval scans which are broken into 
  #                          multiple folders on a secondary interval
  # --- folder_interval: String denoting the interval before creating a 
  #                      secondary folder. Can be 'minutely',
  #                      'half-hourly', 'hourly', 'quarter-daily',
  #                      'half-daily', and 'daily'
  # --- runtime: Total time to run repeats (defaults to 3600)
  # --- interval: Time per run interval. If the run is faster then the 
  #               interval, the program will wait. If it takes longer, 
  #               the next run will start immediately (defaults to 100)
  # --- wait: Number of seconds to wait before checking for the next 
  #           interval (defaults to 1)
  #
  # Examples:
  # - repeat 5 times
  # --- self.repeat_scan(repeat_type='repeat', repeats=5, mine=True, mine_reset=True)
  # - 10 minute (600 seconds) intervals for 1 hour (3600 seconds), with 
  #   15 second waits
  # --- self.repeat_scan(repeat_type='interval', runtime=3600, interval=600, wait=15, mine=True, mine_reset=True)
  # - Hourly (3600 seconds) Intervals broken into daily (86400 seconds) 
  #   reports for 1 week (604800 seconds), with 15 second waits
  # --- self.repeat_scan(repeat_type='multi_folder_interval', folder_interval=86400, runtime=604800, interval=3600, wait=15, mine=True, mine_reset=True)

  def repeat_mine(self, **kwargs):
    if 'repeat_type' in kwargs:
      # Repeat based on anumber of counts
      if kwargs['repeat_type'] == 'repeat':
        repeats = 1
        if 'repeats' in kwargs:
          repeats = kwargs['repeats']
        run = 0
        while run < repeats:
          run += 1
          self.revision_id = 'Run # ' + str(run)
          self.programmatic_run(**kwargs)
      # Interval-based calls
      if kwargs['repeat_type'] == 'interval':
        runtime = 3600
        interval = 100
        wait = 10
        if 'runtime' in kwargs:
          runtime = kwargs['runtime']
        if 'interval' in kwargs:
          interval = kwargs['interval']
        if 'wait' in kwargs:
          wait = kwargs['wait']
        stop_time = time.time() + runtime
        while time.time() < stop_time:
          stop_interval = time.time() + interval
          self.revision_id = time.strftime('%H:%M:%S-%d-%m-%Y',time.localtime())
          self.programmatic_run(**kwargs)
          while time.time() < stop_interval:
            if 'verbose' in kwargs and kwargs['verbose']:
              self.pprint(str(stop_interval - time.time()) + ' seconds until next run, waiting ' + str(wait) + ' seconds before checking the time again.')
            time.sleep(wait)
      # Multi-Folder Interval scan: write data into multiple folders according to a set interval
      if kwargs['repeat_type'] == 'multi_folder_interval':
        folder_format = '%Y-%m-%d'
        folder_interval = 86400
        if kwargs['folder_interval']:
          if kwargs['folder_interval'] == 'minutely':
            folder_format = '%Y-%m-%d_%H-%M'
          if kwargs['folder_interval'] == 'hourly':
            folder_format = '%Y-%m-%d_%H'
          if kwargs['folder_interval'] == 'half-daily':
            folder_format = '%Y-%m-%d_%p'
          if kwargs['folder_interval'] == 'daily':
            folder_format = '%Y-%m-%d'
        runtime = 86400
        interval = 100
        wait = 10
        if 'runtime' in kwargs:
          runtime = kwargs['runtime']
        if 'interval' in kwargs:
          interval = kwargs['interval']
        if 'wait' in kwargs:
          wait = kwargs['wait']
        stop_time = time.time() + runtime
        folder_root = self.settings['report_path']
        try:
            mkdir(folder_root)
        except:
          pass
        while time.time() < stop_time:
          folder_name = time.strftime(folder_format,time.localtime())
          folder_path = folder_root + time.strftime(folder_format,time.localtime()) + '/'
          try:
            mkdir(folder_path)
          except:
            pass
          self.settings['report_path'] = folder_path
          while time.strftime(folder_format,time.localtime()) == folder_name:
            stop_interval = time.time() + interval
            self.revision_id = time.strftime('%H:%M:%S-%d-%m-%Y',time.localtime())
            self.programmatic_run(**kwargs)
            self.programmatic_run(url_reset=True)
            while time.time() < stop_interval:
              if 'verbose' in kwargs and kwargs['verbose']:
                self.pprint(str(stop_interval - time.time()) + ' seconds until next run, waiting ' + str(wait) + ' seconds before checking the time again.')
              time.sleep(wait)
          #Once we get to the end of this folder's time, reset the data
          self.programmatic_run(mine_reset=True)
    else:
      print("Repeat scan was attempted, but we don't have the right settings!")
