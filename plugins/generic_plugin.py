import re
import os
import os.path
import time
import csv
import json

# This is meant to serve as an empty object to reference, as a default item.
class Generic():
  pass

## GenericPlugin: Add a description here
#
# Add more top-level details here.
#
class GenericPlugin():
  # More often than not, you need to reference a parser or miner object.
  parser_obj = Generic()
  miner_obj = Generic()
  # Plugins often have settings. Extend the settings dict here.
  settings = {}
  # If your plugin has data, add it to a data dict.
  data = {}
  
  # OK, let's define the plugin functions. You may call your internal 
  # functions from these. Expected keyword arguments have also been added.

  ######################################################################
  ## Miner Plugins - called during site_miner.mine_site() to modify the url_list (preprocess)
  # or to process data without calling site_miner.process_data() (postprocess)
  def mine_preprocess(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
  
  # This is called during mining according to the miner's frequency. It's a great time to
  # call an export function to do periodic saves/updates
  def mine_periodic_update(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']

  def mine_postprocess(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']

  ######################################################################
  ## Parser Plugins - Used with site_miner.process_url to parse responses made by each 
  # HTTP GET request to the URLs in the url_list.
  
  # Works with the parser to process found tags. Includes the found HTML tag and tag attributes
  def url_processor_tag(self, tag, attrs, **kwargs):
    if 'parser_obj' in kwargs:
      self.parser_obj = kwargs['parser_obj']
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
  
  # Works with the parser to process data found between tags.
  def url_processor_data(self, data, **kwargs):
    if 'parser_obj' in kwargs:
      self.parser_obj = kwargs['parser_obj']
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']

  # The actual parser callback. This takes in the *text* of the response. Based on Python lxml
  # The request data comes from the Python requests module.
  def url_processor_parse(self, data, **kwargs):
    if 'tag_parser_list' in kwargs:
      self.tag_parser_list = kwargs['tag_parser_list']
    if 'data_parser_list' in kwargs:
      self.data_parser_list = kwargs['data_parser_list']
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
    if 'active_url' in kwargs:
      self.active_url = kwargs['active_url']

  # Response data post-processor. Takes in the entire response object, which can either
  # be used, or the URL fed to a non-requests scraper like Ghost.py or Selenium
  def url_processor_postprocess(self, resp, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']

  ######################################################################
  ## Import Plugins - used with site_miner.import_data() to import data from files.
  def import_data_process(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']

  def import_data_postprocess(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']

  ######################################################################
  ## Report callbacks - used with site_miner.generate_report() for exporting report data
  def report_data_preprocess(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']

  def report_data_process(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']

  def report_data_postprocess(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']

  ######################################################################
  ## Data Processor callbacks - used with site_miner.process_data() to process miner data
  # or to process data contained in the plugin itself.
  def process_data(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']

  ######################################################################
  ## Export callbacks - used with site_miner.export_data() to export data to files.
  # You can also think of these as a "save" function.
  def export_data_preprocess(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']

  def export_data_process(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']

  def export_data_postprocess(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
