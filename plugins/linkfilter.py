import re
import os
import os.path
import json
import time
import csv

class Generic():
  pass

## LinkFilter: Filters discovered links
class LinkFilter():
  parser_obj = Generic()
  miner_obj = Generic()
  settings = {}
  settings['require'] = []
  settings['exclude'] = []
  settings['include'] = []
  settings['include_override'] = []
  settings['prioritize'] = []
  settings['allow_subdomains'] = (False, 'example.com')
  settings['section_position'] = 3
  settings['report_methods'] = ['d3_chart']
  settings['max_depth'] = False

  def is_file(self, url):
    ext = re.findall('\.(\w\w)(?!.+\.)(?!.+\/)(?!\/)(?!\.)', url)
    split_url = url.split('/')
    if len(ext) == 0 or len(split_url) == 3:
      return False
    elif ext[0] == 'ph' or ext[0] == 'as' or ext[0] == 'ht':
      ext2 = re.findall('\.(\w\w\w)(?!.+\.)(?!.\/)(?!\/)(?!\.)', url)
      if ext2 and (ext2[0] == 'php' or ext2[0] == 'asp' or ext2[0] == 'ash' or ext2[0] == 'htm'):
        return False
      else:
        return True
    else:
      return True

  def is_external(self, url, domain):
    if domain in url or (self.settings['allow_subdomains'][0] and self.settings['allow_subdomains'][1] in url):
      return False
    else:
      return True

  def is_excluded(self, url, exclude_list):
    if exclude_list:
      ret = False
      for item in exclude_list:
        if item in url:
          ret = True
      return ret
    else:
      return False
  
  # Determines if a URL is included, and therefore eligible to override being excluded
  def is_included(self, url, include_list, include_override_list):
    if include_list:
      ret = False
      for item in include_list:
        if item in url:
          # The URL is 'included,' but let's check that there isn't an override.
          ret = True
          if include_override_list:
            for i in include_override_list:
              if i in url:
                ret = False
      return ret
    else:
      return False

  def is_prioritized(self, url, priority_list):
    if priority_list:
      ret = False
      for item in priority_list:
        if item in url:
          ret = True
      return ret
    else:
      return False

  def is_required(self, url, require_list):
    # The base URL is always OK
    if url == self.miner_obj.settings['domain'] or url == self.miner_obj.settings['domain'] + '/':
      return True
    elif require_list:
      ret = False
      for item in require_list:
        if isinstance(item, list):
          chk = True
          for i in item:
            if i not in url:
              chk = False
          if chk and not ret:
            ret = chk
        elif isinstance(item, str):
          if item in url:
            ret = True
        else:
          pass
      return ret
    else:
      return True
  
  def clean_relative_url(self, url, domain):
    try:
      # Remove spaces first
      url = url.replace(' ', '')
      if 'http://' in url or 'https://' in url:
        return url
      elif '//' in url and not ('http:' in url or 'https:' in url):
        return 'http:' + url
      elif len(url) >= 1 and url[0] == '/':
        return domain + url
      else:
        return domain + '/' + url
    except:
      print('Broken URL: ' + str(url))
      return False

  # Gets the section from the URL
  def get_url_section(self, url, position):
    if position >= 0:
      sections = url.split('/')
      if position < len(sections):
        if sections[position]:
          return sections[position]
        else:
          return 'none'
      else:
        return 'none'
    else:
      return False

  # Add URL type and source data
  def add_url_data(self, url, url_type, option):
    if self.miner_obj.settings[option]:
      if url not in self.miner_obj.settings['url_list'][url_type]:
        # It's assumed basic URL data has been added at this point. Check back later if we get scan errors
        if 'id' not in self.miner_obj.mine_data[url][self.miner_obj.revision_id]:
          if self.is_prioritized(url, self.settings['prioritize']):
            self.miner_obj.settings['url_list'][url_type] = [url] + self.miner_obj.settings['url_list'][url_type]
          else:
            self.miner_obj.settings['url_list'][url_type].append(url)
      self.miner_obj.mine_data[url][self.miner_obj.revision_id]['type'] = url_type
      self.miner_obj.mine_data[url][self.miner_obj.revision_id]['source'] = [self.parser_obj.active_url]
      self.miner_obj.mine_data[url][self.miner_obj.revision_id]['source_count'] = 1
      section = self.get_url_section(url, self.settings['section_position'])
      if section:
        self.miner_obj.mine_data[url][self.miner_obj.revision_id]['section'] = section
      # Determine depth and add the depth
      #~ print(url)
      #~ print(self.parser_obj.active_url)
      #~ print(url != self.miner_obj.settings['domain'])
      #~ print(url != self.miner_obj.settings['domain'] + '/')
      #~ print('depth' in self.miner_obj.mine_data[self.parser_obj.active_url][self.miner_obj.revision_id])
      if url != self.miner_obj.settings['domain'] and url != self.miner_obj.settings['domain'] + '/':
        if not 'depth' in self.miner_obj.mine_data[self.parser_obj.active_url][self.miner_obj.revision_id]:
          self.miner_obj.mine_data[self.parser_obj.active_url][self.miner_obj.revision_id]['depth'] = 0
        if not 'depth' in self.miner_obj.mine_data[url][self.miner_obj.revision_id]:
          depth = self.miner_obj.mine_data[self.parser_obj.active_url][self.miner_obj.revision_id]['depth'] + 1
          if not self.settings['max_depth'] or depth <= self.settings['max_depth']:
            self.miner_obj.mine_data[url][self.miner_obj.revision_id]['depth'] = depth
          else:
            #~ print(url)
            #~ print(depth)
            del(self.miner_obj.mine_data[url][self.miner_obj.revision_id])
            self.miner_obj.settings['url_list'][url_type].remove(url)
      elif url == self.miner_obj.settings['domain'] + '/':
        self.miner_obj.mine_data[url][self.miner_obj.revision_id]['depth'] = 0
      elif url == self.miner_obj.settings['domain']:
        self.miner_obj.mine_data[url][self.miner_obj.revision_id]['depth'] = 0
      else:
        print(url)
        del(self.miner_obj.mine_data[url][self.miner_obj.revision_id])
    else:
      print(url)
      del(self.miner_obj.mine_data[url])

  # Generate URL Data Report in a d3 JSON chart format
  def d3_chart_report(self):
    output_data = {'nodes':[],'links':[]}
    url_count = 0
    for url in self.miner_obj.mine_data:
      if self.miner_obj.revision_id in self.miner_obj.mine_data[url] and \
      'id' in self.miner_obj.mine_data[url][self.miner_obj.revision_id] and \
      isinstance(self.miner_obj.mine_data[url][self.miner_obj.revision_id]['id'], int):
        url_count += 1
    output_data['nodes'] = [0] * (url_count)
    for url in self.miner_obj.mine_data:
      if self.miner_obj.revision_id in self.miner_obj.mine_data[url] and \
      'id' in self.miner_obj.mine_data[url][self.miner_obj.revision_id] and \
      isinstance(self.miner_obj.mine_data[url][self.miner_obj.revision_id]['id'], int) and \
      'depth' in self.miner_obj.mine_data[url][self.miner_obj.revision_id]:
        urlid = self.miner_obj.mine_data[url][self.miner_obj.revision_id]['id'] - 1
        depth = self.miner_obj.mine_data[url][self.miner_obj.revision_id]['depth']
        output_data['nodes'][urlid] = {'name' : url + ' - Level ' + str(depth),'group' : depth}
        if 'source' in self.miner_obj.mine_data[url][self.miner_obj.revision_id] and self.miner_obj.mine_data[url][self.miner_obj.revision_id]['source']:
          for source in self.miner_obj.mine_data[url][self.miner_obj.revision_id]['source']:
            if self.miner_obj.revision_id in self.miner_obj.mine_data[source] and \
            'id' in self.miner_obj.mine_data[source][self.miner_obj.revision_id]:
              sourceid = self.miner_obj.mine_data[source][self.miner_obj.revision_id]['id'] - 1
              output_data['links'].append({'source' : sourceid, 'target' : urlid, 'value': 1})
    # Assign position 0 if it wasn't assigned already
    if output_data['nodes'] and output_data['nodes'][0] == 0:
      output_data['nodes'][0] = {'name':self.miner_obj.settings['domain'],'group': 0}
    # Create the output file
    with open(os.path.abspath('.') + '/' + self.miner_obj.settings['report_path'] + 'd3_linkfilter.json', 'w', newline='') as f:
      json.dump(output_data, f)

  ######################################################################
  # Plugin callbacks
  ######################################################################

  # Periodic Update: Export Data and generate reports
  def mine_periodic_update(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if self in self.miner_obj.plugin_list['report']['process']:
        self.report_data_process(miner_obj=self.miner_obj)

  # Filter tag: look for new links
  def url_processor_tag(self, tag, attrs, **kwargs):
    if 'parser_obj' in kwargs:
      self.parser_obj = kwargs['parser_obj']
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
    if attrs and self.miner_obj:
      for item in attrs:
        if item[0] == 'href' or item[0] == 'src':
          clean = self.clean_relative_url(item[1], self.miner_obj.settings['domain'])
          # Process the URL if it is:
          # - not excluded
          # - included and not matching an override
          # - required
          if clean and (not self.is_excluded(clean, self.settings['exclude']) or \
          self.is_included(clean, self.settings['include'], self.settings['include_override'])) \
          and self.is_required(clean, self.settings['require']):
            if clean in self.miner_obj.mine_data and \
            self.miner_obj.revision_id in self.miner_obj.mine_data[clean]:
              if 'type' in self.miner_obj.mine_data[clean][self.miner_obj.revision_id] and \
              'source' in self.miner_obj.mine_data[clean][self.miner_obj.revision_id] and \
              'source_count' in self.miner_obj.mine_data[clean][self.miner_obj.revision_id]:
                if 'source_count' not in self.miner_obj.mine_data[clean][self.miner_obj.revision_id]:
                  self.miner_obj.mine_data[clean][self.miner_obj.revision_id] = {'source_count' : 1, 'source' : [self.parser_obj.active_url]}
                elif self.miner_obj.mine_data[clean][self.miner_obj.revision_id]['source_count'] <= 15:
                  self.miner_obj.mine_data[clean][self.miner_obj.revision_id]['source'].append(self.parser_obj.active_url)
                  self.miner_obj.mine_data[clean][self.miner_obj.revision_id]['source_count'] += 1
                else:
                  self.miner_obj.mine_data[clean][self.miner_obj.revision_id]['source_count'] += 1
              # We can run a few tests to see if this is needed, as it is now covered in the 'else' statement below.
              else:
                if self.is_file(clean):
                  self.add_url_data(clean, 'file', 'mine_file')
                elif self.is_external(clean, self.miner_obj.settings['domain']):
                  self.add_url_data(clean, 'external', 'mine_external')
                elif not self.is_file(clean) and not self.is_external(clean, self.miner_obj.settings['domain']):
                  self.add_url_data(clean, 'internal', 'mine_internal')
            
            else:
              if self.is_file(clean) and self.miner_obj.settings['mine_file']:
                self.miner_obj.add_empty_url_data(clean)
                self.add_url_data(clean, 'file', 'mine_file')
              elif self.is_external(clean, self.miner_obj.settings['domain']) and self.miner_obj.settings['mine_external']:
                self.miner_obj.add_empty_url_data(clean)
                self.add_url_data(clean, 'external', 'mine_external')
              elif not self.is_file(clean) and not self.is_external(clean, self.miner_obj.settings['domain'])and self.miner_obj.settings['mine_internal']:
                self.miner_obj.add_empty_url_data(clean)
                self.add_url_data(clean, 'internal', 'mine_internal')
    
  # Generate URL Data Report
  def report_data_process(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if 'd3_chart' in self.settings['report_methods']:
        self.d3_chart_report()
