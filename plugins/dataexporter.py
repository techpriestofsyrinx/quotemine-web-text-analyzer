import re
import os
import os.path
import time
import csv
import json

class Generic():
  pass

## LinkFilter: Filters discovered links
class DataExporter():
  parser_obj = Generic()
  miner_obj = Generic()
  settings = {}
  settings['export_methods'] = ['csv_data', 'json_data']
  settings['json_export_filename'] = 'raw_export.dat'
  settings['csv_export_filename'] = 'url_data.csv'

  # Generate URL Data Report in CSV format, from miner's URL data
  def csv_data_report(self):
    if self.miner_obj.mine_data:
      with open(os.path.abspath('.') + '/' + self.miner_obj.settings['report_path'] + self.settings['csv_export_filename'], 'w', newline='') as f:
        writer = csv.writer(f)
        mine_data = self.miner_obj.mine_data
        # Build header row
        rows = []
        head_row = ['URL', 'URL ID', 'Revision']
        for data in mine_data:
          for rev in mine_data[data]:
            if rev != 'id':
              for item in mine_data[data][rev]:
                if not item in head_row:
                  head_row.append(item)
        rows.append(head_row)
        # Build rows
        for data in mine_data:
          for rev in mine_data[data]:
            #print(rev)
            if rev != 'id':
              row = [data]
              if 'id' in mine_data[data]:
                row.append(mine_data[data]['id'])
              else:
                row.append('')
              row.append(rev)
              for item in head_row:
                if item in mine_data[data][rev] and item not in ['URL', 'URL ID', 'Revision']:
                  row.append(mine_data[data][rev][item])
                elif item not in ['URL', 'URL ID', 'Revision']:
                  row.append('')
              rows.append(row)
        # Write to file
        for row in rows:
          try:
            writer.writerow(row)
          except:
            pass

  def mine_periodic_update(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if self in self.miner_obj.plugin_list['export']['process']:
        self.export_data_process(miner_obj=self.miner_obj)

  def export_data_process(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if 'csv_data' in self.settings['export_methods']:
        self.csv_data_report()
      if 'json_data' in self.settings['export_methods']:
        self.csv_data_report()
      data = self.miner_obj.mine_data
      with open(os.path.abspath('.') + '/' + self.miner_obj.settings['report_path'] + self.settings['json_export_filename'] , 'w') as f:
        json.dump(data, f)
