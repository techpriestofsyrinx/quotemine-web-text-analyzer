import re
import os
import os.path
import time
import csv
import json

# This is meant to serve as an empty object to reference, as a default item.
class Generic():
  pass

## URLListBuilder: Tool to build URL lists for a 
#
# Add more top-level details here.
#
class URLListBuilder():
  # More often than not, you need to reference a parser or miner object.
  parser_obj = Generic()
  miner_obj = Generic()
  # Plugins often have settings. Extend the settings dict here.
  settings = {}
  settings['list_builders'] = ['numeric_builder', 'url_data_builder', 'url_depth_builder']
  settings['numeric_builder'] = {}
  settings['numeric_builder']['base_url'] = 'http://www.example.com?param='
  settings['numeric_builder']['end_url'] = '&param2=whatever'
  settings['numeric_builder']['category'] = 'internal'
  settings['numeric_builder']['range_start'] = 1
  settings['numeric_builder']['range_stop'] = 2
  
  settings['url_data_builder'] = {}
  settings['url_data_builder']['method'] = 'exclude'
  settings['url_data_builder']['data_type'] = 'status'
  settings['url_data_builder']['include'] = ['Scan Error', '404']
  settings['url_data_builder']['exclude'] = ['200']

  # url_depth_builder is a variation of the url_data_builder, focusing 
  # specfically on link depth
  settings['url_depth_builder'] = {}
  settings['url_depth_builder']['max_depth'] = 1
  settings['url_depth_builder']['max_section_length'] = 5
  settings['url_depth_builder']['depth_label'] = 'depth'
  settings['url_depth_builder']['exclude'] = ''

  def add_to_url_list(self, url, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      typ = ''
      for rev in self.miner_obj.mine_data[url]:
        if rev != 'id':
          typ = self.miner_obj.mine_data[url][rev]['type']
      if typ and url not in self.miner_obj.settings['url_list'][typ]:
        self.miner_obj.settings['url_list'][typ].append(url)
  
  # OK, let's define the plugin functions. You may call your internal 
  # functions from these. Expected keyword arguments have also been added.

  ######################################################################
  ## Miner Plugins - called during site_miner.mine_site() to modify the url_list (preprocess)
  # or to process data without calling site_miner.process_data() (postprocess)
  def mine_preprocess(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if 'numeric_builder' in self.settings['list_builders']:
        for i in range(self.settings['numeric_builder']['range_start'], self.settings['numeric_builder']['range_stop']):
          url = self.settings['numeric_builder']['base_url'] + str(i) + self.settings['numeric_builder']['end_url']
          self.miner_obj.settings['url_list'][self.settings['numeric_builder']['category']].append(url)

  ######################################################################
  ## Import Plugins - used with site_miner.import_data() to import data from files.

  def import_data_postprocess(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if 'url_data_builder' in self.settings['list_builders']:
        data_type = self.settings['url_data_builder']['data_type']
        method = self.settings['url_data_builder']['method']
        inc = self.settings['url_data_builder']['include']
        exc = self.settings['url_data_builder']['exclude']
        for url in self.miner_obj.mine_data:
          for rev in self.miner_obj.mine_data[url]:
            if rev != 'id':
              if data_type in self.miner_obj.mine_data[url][rev] and 'type' in self.miner_obj.mine_data[url][rev]:
                data = str(self.miner_obj.mine_data[url][rev][data_type])
                if method == 'include':
                  if isinstance(inc, str) and inc in data:
                    self.add_to_url_list(url, miner_obj= self.miner_obj)
                  elif isinstance(inc, list):
                    go = False
                    for i in inc:
                      if i in data:
                        go = True
                    if go:
                      self.add_to_url_list(url, miner_obj= self.miner_obj)
                elif method == 'exclude':
                  if isinstance(exc, str) and not exc in data:
                    self.add_to_url_list(url, miner_obj= self.miner_obj)
                  elif isinstance(exc, list):
                    go = True
                    for e in exc:
                      if e in data:
                        go = False
                    if go:
                      self.add_to_url_list(url, miner_obj= self.miner_obj)

      elif 'url_depth_builder' in self.settings['list_builders']:
        md = int(self.settings['url_depth_builder']['max_depth'])
        ms = int(self.settings['url_depth_builder']['max_section_length'])
        label = self.settings['url_depth_builder']['depth_label']
        exc = self.settings['url_depth_builder']['exclude']
        for url in self.miner_obj.mine_data:
          for rev in self.miner_obj.mine_data[url]:
            if rev != 'id':
              if label in self.miner_obj.mine_data[url][rev] and 'type' in self.miner_obj.mine_data[url][rev]:
                d = int(self.miner_obj.mine_data[url][rev][label])
                s = len(url.split('/'))
                if d <= md and s <= ms and (not exc or exc not in url):
                  self.add_to_url_list(url, miner_obj= self.miner_obj)
