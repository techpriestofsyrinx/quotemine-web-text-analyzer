import re
import os
import os.path
import time
import csv
import json

# This is meant to serve as an empty object to reference, as a default item.
class Generic():
  pass

## HeaderFilter: Add a description here
#
# Add more top-level details here.
#
class HeaderFilter():
  # More often than not, you need to reference a parser or miner object.
  parser_obj = Generic()
  miner_obj = Generic()
  # Plugins often have settings. Extend the settings dict here.
  settings = {}
  settings['require'] = []
  settings['exclude'] = []
  settings['add_data_to_main'] = True
  
  # Import/Export settings
  settings['report_filename'] = 'header_report.csv'
  settings['import_filename'] = 'header_data.dat'
  settings['export_filename'] = 'header_data.dat'
  # If your plugin has data, add it to a data dict.
  data = {}
  
  ######################################################################
  ## Helper Functions
  # Check if one object contains any substring from a list
  def contains_any(self, needles, haystack):
    ret = False
    for needle in needles:
      if needle in haystack:
        ret = True
    return ret

  ######################################################################
  ## Miner Plugins - called during site_miner.mine_site() to modify the url_list (preprocess)
  # or to process data without calling site_miner.process_data() (postprocess)

  # This is called during mining according to the miner's frequency. It's a great time to
  # call an export function to do periodic saves/updates
  def mine_periodic_update(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if self in self.miner_obj.plugin_list['report']['process']:
        self.report_data_process(miner_obj=self.miner_obj)
      if self in self.miner_obj.plugin_list['export']['process']:
        self.export_data_process(miner_obj=self.miner_obj)

  ######################################################################
  ## Parser Plugins - Used with site_miner.process_url to parse responses made by each 
  # HTTP GET request to the URLs in the url_list.

  # Response data post-processor. Takes in the entire response object, which can either
  # be used, or the URL fed to a non-requests scraper like Ghost.py or Selenium
  def url_processor_postprocess(self, resp, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      for header in resp.headers:
        # Check whether headers are required, or not excluded
        if not self.settings['require'] or \
        self.contains_any(self.settings['require'], header):
          if not self.settings['exclude'] or \
          not self.contains_any(self.settings['exclude'], header):
            # Add data to internal data object. Build the dicts first
            if resp.init_url not in self.data:
              self.data[resp.init_url] = {}
            if self.miner_obj.revision_id not in self.data[resp.init_url]:
              self.data[resp.init_url][self.miner_obj.revision_id] = {}
            self.data[resp.init_url][self.miner_obj.revision_id][header] = resp.headers[header]
            # Add to miner if we have the necessary
            if self.settings['add_data_to_main']:
              if resp.init_url not in self.miner_obj.mine_data:
                self.miner_obj.mine_data[resp.init_url] = {}
              if self.miner_obj.revision_id not in self.miner_obj.mine_data[resp.init_url]:
                self.miner_obj.mine_data[resp.init_url][self.miner_obj.revision_id] = {}
              self.miner_obj.mine_data[resp.init_url][self.miner_obj.revision_id][header] = resp.headers[header]

  # Generate Report
  def report_data_process(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      with open(os.path.abspath('.') + '/' + self.miner_obj.settings['report_path'] + self.settings['report_filename'] , 'w') as f:
        writer = csv.writer(f)
        mine_data = self.data
        # Build header row
        rows = []
        head_row = ['URL', 'Revision']
        for data in mine_data:
          for rev in mine_data[data]:
            if rev != 'id':
              for item in mine_data[data][rev]:
                if not item in head_row:
                  head_row.append(item)
        rows.append(head_row)
        # Build rows
        for data in mine_data:
          for rev in mine_data[data]:
            row = [data, rev]
            for item in head_row:
              if item in mine_data[data][rev] and item not in ['URL', 'Revision']:
                row.append(mine_data[data][rev][item])
              elif item not in ['URL', 'Revision']:
                row.append('')
            rows.append(row)
        # Write to file
        for row in rows:
          try:
            writer.writerow(row)
          except:
            pass
       

  # Import Data
  def import_data_process(self,**kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      with open(os.path.abspath('.') + '/' + self.miner_obj.settings['report_path'] + self.settings['import_filename'] , 'r') as f:
        data = json.load(f)
        self.data = data

  # Export Data
  def export_data_process(self,**kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      data = self.data
      with open(os.path.abspath('.') + '/' + self.miner_obj.settings['report_path'] + self.settings['export_filename'] , 'w') as f:
        json.dump(data, f)
