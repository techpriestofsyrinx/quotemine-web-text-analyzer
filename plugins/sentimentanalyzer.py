import re
import os
import os.path
import time
import csv
import json
import requests

# This is meant to serve as an empty object to reference, as a default item.
class Generic():
  pass

## GenericPlugin: Add a description here
#
# Add more top-level details here.
#
class SentimentAnalyzer():
  # More often than not, you need to reference a parser or miner object.
  parser_obj = Generic()
  miner_obj = Generic()
  settings = {}
  ## API settings. Determine when to call the AlchemyLanguage API, and how to call it
  # URL: URL must contain one substring within the list
  # Text: Page text must contain one substring within the list
  # URL and Text operate on AND basis
  settings['api_call_criteria'] = {}
  settings['api_call_criteria']['url'] = []
  settings['api_call_criteria']['text'] = []
  # API endpoint settings
  settings['api_endpoint'] = 'http://gateway-a.watsonplatform.net/calls/url/URLGetRankedNamedEntities'
  settings['api_key'] = 'your_key'
  settings['api_args'] = [('sentiment', '1'),('outputMode', 'json')]
  # Entity settings
  settings['entities'] = {}
  settings['entities']['add_to_mine_data'] = []
  settings['entities']['sentiment_data'] = ['type', 'score']
  settings['entities']['require'] = []
  settings['entities']['exclude'] = []
  # Import/Export settings
  settings['report_filename'] = 'sentiment_report.csv'
  settings['import_filename'] = 'sentiment_report.dat'
  settings['export_filename'] = 'sentiment_report.dat'
  ## Report Settings
  # If your plugin has data, add it to a data dict.
  data = {}
  
  # OK, let's define the plugin functions. You may call your internal 
  # functions from these. Expected keyword arguments have also been added.

  ######################################################################
  ## Helper Functions
  # Check if one object contains any substring from a list
  def contains_any(self, needles, haystack):
    ret = False
    for needle in needles:
      if needle in haystack:
        ret = True
    return ret

  ######################################################################
  ## Miner Plugins - called during site_miner.mine_site() to modify the url_list (preprocess)
  # or to process data without calling site_miner.process_data() (postprocess)

  # This is called during mining according to the miner's frequency. It's a great time to
  # call an export function to do periodic saves/updates
  def mine_periodic_update(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if self in self.miner_obj.plugin_list['report']['process']:
        self.report_data_process(miner_obj=self.miner_obj)
      if self in self.miner_obj.plugin_list['export']['process']:
        self.export_data_process(miner_obj=self.miner_obj)

  ######################################################################
  ## Parser Plugins - Used with site_miner.process_url to parse responses made by each 
  # HTTP GET request to the URLs in the url_list.

  # Response data post-processor. Takes in the entire response object, which can either
  # be used, or the URL fed to a non-requests scraper like Ghost.py or Selenium
  def url_processor_postprocess(self, resp, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      # First, do we match the URL criteria?
      if not self.settings['api_call_criteria']['url'] or \
      self.contains_any(self.settings['api_call_criteria']['url'], resp.init_url):
        if not self.settings['api_call_criteria']['text'] or \
        self.contains_any(self.settings['api_call_criteria']['text'], resp.text):
          api_url = self.settings['api_endpoint'] + '?apikey=' + self.settings['api_key'] + '&url=' + resp.init_url
          for arg in self.settings['api_args']:
            api_url = api_url + '&' + arg[0] + '=' + arg[1]
          res = requests.get(api_url)
          data = json.loads(res.text)
          if 'entities' in data:
            for d in data['entities']:
              # Check that we have the complete data set. If so, process it for adding to the data object
              if 'disambiguated' in d and 'name' in d['disambiguated'] and 'type' in d and 'sentiment' in d:
                # Add sentiment data to miner's main data if it's in the include list
                if d['disambiguated']['name'] in self.settings['entities']['add_to_mine_data']:
                  for dat in self.settings['entities']['sentiment_data']:
                    if dat in d['sentiment']:
                      self.miner_obj.mine_data[resp.init_url][self.miner_obj.revision_id][d['disambiguated']['name'] + ' sentiment ' + dat] = d['sentiment'][dat]
                # Now, check whether the 'required' list is active and if the item is required, or if the item is excluded from data collection
                if not self.settings['entities']['require'] or \
                self.contains_any(self.settings['entities']['require'], d['disambiguated']['name']):
                  if not self.settings['entities']['exclude'] or \
                  not self.contains_any(self.settings['entities']['exclude'], d['disambiguated']['name']):
                    # Add data to internal data object. Build the dicts first
                    if resp.init_url not in self.data:
                      self.data[resp.init_url] = {}
                    if self.miner_obj.revision_id not in self.data[resp.init_url]:
                      self.data[resp.init_url][self.miner_obj.revision_id] = {}
                    self.data[resp.init_url][self.miner_obj.revision_id][d['disambiguated']['name']] = {'name': d['disambiguated']['name'], 'type': d['type'], 'sentiment': d['sentiment']}

  ######################################################################
  ## Report callbacks - used with site_miner.generate_report() for exporting report data

  def report_data_process(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      with open(os.path.abspath('.') + '/' + self.miner_obj.settings['report_path'] + self.settings['report_filename'] , 'w') as f:
        writer = csv.writer(f)
        head_row = ['URL', 'Revision', 'Entity', 'Entity type']
        for i in self.settings['entities']['sentiment_data']:
          head_row.append('Sentiment ' + i)
        writer.writerow(head_row)
        for url in self.data:
          for revision in self.data[url]:
            for item in self.data[url][revision]:
              row = [url, revision, item, self.data[url][revision][item]['type']]
              for i in self.settings['entities']['sentiment_data']:
                if i in self.data[url][revision][item]['sentiment']:
                  row.append(self.data[url][revision][item]['sentiment'][i])
                else:
                  row.append('')
              writer.writerow(row)

  # Import Data
  def import_data_process(self,**kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      with open(os.path.abspath('.') + '/' + self.miner_obj.settings['report_path'] + self.settings['import_filename'] , 'r') as f:
        data = json.load(f)
        self.data = data

  # Export Data
  def export_data_process(self,**kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      data = self.data
      with open(os.path.abspath('.') + '/' + self.miner_obj.settings['report_path'] + self.settings['export_filename'] , 'w') as f:
        json.dump(data, f)
