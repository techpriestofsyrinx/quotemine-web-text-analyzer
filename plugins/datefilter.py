import re
import os
import os.path
import time
import csv
from bs4 import BeautifulSoup

class Generic():
  pass

## LinkFilter: Filters discovered links
class DateFilter():
  parser_obj = Generic()
  miner_obj = Generic()
  settings = {}
  settings['method'] = 'url' # Use only one of 'url' , 'tag' , or 'select'
  settings['date_search'] = {}
  settings['date_search']['type'] = 'entire_date' # Use either 'entire_date' for 12/03/2015 type dates, or 'partial_date' for extracting Dec 03, 2015 type dates by parts
  settings['date_search']['day_regex'] = '(?<!\d)(\d\d|\d)(?!\d)'
  settings['date_search']['month_dict'] = {'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04', 'May': '05', 'Jun': '06', 'Jul': '07', 'Aug': '08', 'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12'}
  settings['date_search']['year_regex'] = '(?<!\d)\d\d\d\d(?!\d)'
  settings['date_search']['entire_date_regex'] = '(\d\d\d\d|\d\d)-(\d\d|\d)-(\d\d|\d)' # '(\d\d\d\d|\d\d)\/(\d\d|\d)\/(\d\d|\d)'
  # URL: Get the date out of the URL
  settings['url'] = {}
  settings['url']['require'] = []
  settings['url']['exclude'] = []
  settings['url']['allow_subdomains'] = (False, 'example.com')
  # Tag: get a tag, identify the tag with a certain attribute value, and use its data
  settings['tag'] = {}
  settings['tag']['tag'] = 'none'
  settings['tag']['attr'] = ('class', 'timestamp')
  # Tag attribute: get a tag, identify the tag with a certain attribute value, and use data in a specific data attribute
  settings['select'] = 'time'

  # TODO: Document this, replace dash/slach matches with single regex statement
  def url_to_date(self, url):
    date = ''
    # if date of type yyyy-mm-dd
    dashmatch = re.findall('(\d\d\d\d|\d\d)-(\d\d|\d)-(\d\d|\d)', url)
    # if date of type yyyy/mm/dd
    slashmatch = re.findall('(\d\d\d\d|\d\d)\/(\d\d|\d)\/(\d\d|\d)', url)
    if dashmatch:
      if isinstance(dashmatch[0], tuple):
        date = '/'.join(map(str,dashmatch[0]))
      elif isinstance(dashmatch[0], str):
        date = dashmatch[0].replace('-','/')
    elif slashmatch:
      if isinstance(slashmatch[0], tuple):
        date = '/'.join(map(str,slashmatch[0]))
      elif isinstance(slashmatch[0], str):
        date = slashmatch[0]
    else:
      date = 'No Date found'
    return date

  # TODO: Document this
  def text_to_date(self, text):
    date = ''
    day = re.findall(self.settings['date_search']['day_regex'], text)
    if day:
      day = day[0]
    months = self.settings['date_search']['month_dict']
    month = ''
    for m in months:
      if m in text:
        month = months[m]
        break
    year = re.findall(self.settings['date_search']['year_regex'], text)
    if year:
      year = year[0]
    if day and month and year:
      date = month + '/' + day + '/' + year
    else:
      date = 'No Date found'
    return date

  def clean_relative_url(self, url, domain):
    url = str(url)
    if 'http://' in url or 'https://' in url:
      return url
    elif '//' in url and not ('http:' in url or 'https:' in url):
      return 'http:' + url
    elif len(url) >= 1 and url[0] == '/':
      return domain + url
    else:
      return domain + '/' + url

  def is_file(self, url):
    ext = re.findall('\.(\w\w)(?!.+\.)(?!.+\/)(?!\/)(?!\.)', url)
    split_url = url.split('/')
    if len(ext) == 0 or len(split_url) == 3:
      return False
    elif ext[0] == 'ph' or ext[0] == 'as' or ext[0] == 'ht':
      ext2 = re.findall('\.(\w\w\w)(?!.+\.)(?!.\/)(?!\/)(?!\.)', url)
      if ext2 and (ext2[0] == 'php' or ext2[0] == 'asp' or ext2[0] == 'ash' or ext2[0] == 'htm'):
        return False
      else:
        return True
    else:
      return True

  def is_external(self, url, domain):
    if domain in url or (self.settings['url']['allow_subdomains'][0] and self.settings['url']['allow_subdomains'][1] in url):
      return False
    else:
      return True

  def is_excluded(self, url, exclude_list):
    if exclude_list:
      ret = False
      for item in exclude_list:
        if item in url:
          ret = True
      return ret
    else:
      return False

  def is_required(self, url, require_list):
    if require_list:
      ret = False
      for item in require_list:
        if item in url:
          ret = True
      return ret
    else:
      return True

  # Look for date in URLs
  def url_processor_tag(self, tag, attrs, **kwargs):
    if self.settings['method'] == 'url':
      if 'parser_obj' in kwargs:
        self.parser_obj = kwargs['parser_obj']
      if 'miner_obj' in kwargs:
        self.miner_obj = kwargs['miner_obj']
        if attrs:
          for item in attrs:
            if item[0] == 'href' or item[0] == 'src':
              clean = self.clean_relative_url(item[1], self.miner_obj.settings['domain'])
              if not self.is_excluded(clean, self.settings['url']['exclude']) and \
              self.is_required(clean, self.settings['url']['require']) and \
              not self.is_external(clean, self.miner_obj.settings['domain']) and \
              not self.is_file(clean):
                if clean in self.miner_obj.mine_data:
                  if self.miner_obj.revision_id not in self.miner_obj.mine_data[clean]:
                    self.miner_obj.add_empty_url_data(clean)
                  if 'date' not in self.miner_obj.mine_data[clean][self.miner_obj.revision_id]:
                    self.miner_obj.mine_data[clean][self.miner_obj.revision_id]['date'] = self.url_to_date(clean)
                elif clean not in self.miner_obj.mine_data:
                  self.miner_obj.add_empty_url_data(clean)
                  self.miner_obj.mine_data[clean][self.miner_obj.revision_id]['date'] = self.url_to_date(clean)

  # Response data post-processor. Takes in the entire response object, which can either
  # be used, or the URL fed to a non-requests scraper like Ghost.py or Selenium
  def url_processor_postprocess(self, resp, **kwargs):
    if self.settings['method'] == 'select':
      if 'miner_obj' in kwargs:
        self.miner_obj = kwargs['miner_obj']
        html = resp.text
        soup = BeautifulSoup(html, 'html.parser')
        for tag in soup.select(self.settings['select']):
          date = self.text_to_date(str(tag))
          self.miner_obj.mine_data[resp.init_url][self.miner_obj.revision_id]['date'] = date
