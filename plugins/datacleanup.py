import re
import os
import os.path
import time
import json
import csv

class Generic():
  pass

## DataCleanup: Cleans unscanned URLs
class DataCleanup():
  miner_obj = Generic()
  ## Cleaning methods:
  # 
  # Basic logic: URLs without a 'status' have not been scanned.
  #
  # 'remove' - Deletes any URL that wasn't scanned
  # 'mark' - Marks Scanned and Unscanned URLs as such
  
  settings = {}
  settings['clean_method'] = 'none' # 'remove' or 'mark'
  
  # Cleans unscanned URLs by removing them from the data. Assumes miner_obj has been set
  def clean_by_removal(self):
    for url in self.miner_obj.mine_data:
      if self.miner_obj.revision_id in self.miner_obj.mine_data[url]:
        if 'status' not in self.miner_obj.mine_data[url][self.miner_obj.revision_id]:
          del(self.miner_obj.mine_data[url][self.miner_obj.revision_id])
        else:
          pass
  # Cleans unscanned URLs by marking them as scanned or unscanned. Assumes miner_obj has been set
  def mark_clean_unclean(self):
    for url in self.miner_obj.mine_data:
      if self.miner_obj.revision_id in self.miner_obj.mine_data[url]:
        if 'status' not in self.miner_obj.mine_data[url][self.miner_obj.revision_id]:
          self.miner_obj.mine_data[url][self.miner_obj.revision_id]['scanned'] = True
        else:
          self.miner_obj.mine_data[url][self.miner_obj.revision_id]['scanned'] = False
  
  def mine_postprocess(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if self.settings['clean_method'] == 'remove':
        self.clean_by_removal()
      elif self.settings['clean_method'] == 'mark':
        self.mark_clean_unclean()
      else:
        pass
      
