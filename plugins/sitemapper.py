import re
import os
import os.path
import time
import json
import csv

class Generic():
  pass

## LinkFilter: Filters discovered links
class SiteMapper():
  miner_obj = Generic()
  settings = {}
  settings['end_page'] = 'index.html'
  settings['generate_methods'] = ['generate_d3_tree']
  settings['revision'] = ''
  settings['import_filename'] = 'sitemapper.json'
  settings['export_filename'] = 'sitemapper.json'
  data = {}

  def strip_domain(self, url, domain):
    if url == domain:
      return ''
    else:
      return url.replace(domain + '/', '')
  
  def strip_dates(self, url):
    date = ''
    # if date of type yyyy-mm-dd
    dashmatch = re.findall('\d\d\d\d-\d\d-\d\d', url)
    # if date of type yyyy/mm/dd
    slashmatch = re.findall('\d\d\d\d\/\d\d\/\d\d', url)
    slashmatch2 = re.findall('\d\d\d\d\/\d\d', url)
    slashmatch3 = re.findall('\d\d\/\d\d\d\d', url)
    slashmatch4 = re.findall('\d\d\/\d\d', url)
    # if date of type mmddyyyy
    numericmatch = re.findall('\d\d\d\d\d\d\d\d', url)
    numericmatch2 = re.findall('\d\d\d\d\d\d', url)
    if dashmatch:
      url = url.replace(dashmatch[0],'yyyy-mm-dd')
    elif slashmatch:
      url = url.replace(slashmatch[0],'yyyy-mm-dd')
    elif slashmatch2:
      url = url.replace(slashmatch2[0],'yyyy-mm')
    elif slashmatch3:
      url = url.replace(slashmatch3[0],'mm-yyyy')
    elif slashmatch4:
      url = url.replace(slashmatch4[0],'yy-mm')
    elif numericmatch:
      url = url.replace(numericmatch[0],'mmddyyyy')
    elif numericmatch2:
      url = url.replace(numericmatch2[0],'mmddyy')
    return url
  
  # Generate network tree. Assumes that the miner_obj has already been set.
  def generate_network_tree(self):
    self.data[self.miner_obj.settings['domain']] = {}
    root = self.data[self.miner_obj.settings['domain']]
    for url in self.miner_obj.mine_data:
      if self.settings['revision'] and self.settings['revision'] in self.miner_obj.mine_data[url]:
        clean = self.strip_dates(self.strip_domain(url, self.miner_obj.settings['domain']))
        sections = clean.split('/')
        previous_section = root
        for s in sections:
          if s == '' or (not self.settings['end_page'] and sections.index(s) == len(sections) - 1) or  \
          (self.settings['end_page'] and sections.index(s) < len(sections) - 1 and sections[sections.index(s) + 1] == self.settings['end_page']) or \
          (self.settings['end_page'] and s == self.settings['end_page']):
            pass
          elif s in previous_section:
            previous_section = previous_section[s]
          else:
            previous_section[s] = {}
            previous_section = previous_section[s]
    
  # Generate network tree for D3 charting format. Assumes that the miner_obj has already been set.
  def generate_d3_tree(self):
    self.data = {'name' : self.miner_obj.settings['domain'], 'children': []}
    root = self.data
    # Build the tree
    for url in self.miner_obj.mine_data:
      clean = self.strip_dates(self.strip_domain(url, self.miner_obj.settings['domain']))
      sections = clean.split('/')
      previous_section = root
      for s in sections:
        if s == '' or (not self.settings['end_page'] and not len(sections) == 1 and sections.index(s) == len(sections) - 1) or  \
        (self.settings['end_page'] and sections.index(s) < len(sections) - 1 and sections[sections.index(s) + 1] == self.settings['end_page']) or \
        (self.settings['end_page'] and s == self.settings['end_page']):
          pass
        else:
          add = True
          i = 0
          for child in previous_section['children']:
            if s == child['name']:
              add = False
              previous_section = previous_section['children'][i]
            else:
              i += 1
          if add:
            previous_section['children'].append({'name': s, 'children': []})
            previous_section = previous_section['children'][i]
            i += 1
    # Cleanup: move single-level children to the "bottom" of the list
    with_children = []
    no_children = []
    for child in self.data['children']:
      if not child['children']:
        no_children.append(child)
      else:
        with_children.append(child)
    self.data['children'] = no_children + with_children

  # process_data callback
  def process_data(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if 'generate_network_tree' in self.settings['generate_methods']:
        self.generate_network_tree()
      elif 'generate_d3_tree' in self.settings['generate_methods']:
        self.generate_d3_tree()

  # Periodic Update: Export Data and generate reports
  def mine_periodic_update(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if self in self.miner_obj.plugin_list['export']['process']:
        self.export_data_process(miner_obj=self.miner_obj)

  # Import Data
  def import_data_process(self,**kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      with open(os.path.abspath('.') + '/' + self.miner_obj.settings['report_path'] + self.settings['import_filename'] , 'r') as f:
        data = json.load(f)
        self.data = data

  # Export Data
  def export_data_process(self,**kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      data = self.data
      with open(os.path.abspath('.') + '/' + self.miner_obj.settings['report_path'] + self.settings['export_filename'] , 'w') as f:
        json.dump(data, f)
