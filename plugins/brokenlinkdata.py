import re
import os
import os.path
import time
import csv

class Generic():
  pass

## BrokenLinkData: Creates a broken link report
class BrokenLinkData():
  parser_obj = Generic()
  miner_obj = Generic()
  settings = {}
  # Search through URL sources, to see if any of the substrings exist
  settings['search'] = []

  def mine_periodic_update(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if self in self.miner_obj.plugin_list['report']['process']:
        self.report_data_process(miner_obj=self.miner_obj)

  # Generate URL Data Report
  def report_data_process(self, **kwargs):
    if 'parser_obj' in kwargs:
      self.parser_obj = kwargs['parser_obj']
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if self.miner_obj.mine_data:
        with open(os.path.abspath('.') + '/' + self.miner_obj.settings['report_path'] + 'broken_link_data.csv', 'w', newline='') as f:
          writer = csv.writer(f)
          mine_data = self.miner_obj.mine_data
          # Build header row
          rows = []
          head_row = ['URL', 'Revision', 'status']
          # Build rows
          for data in mine_data:
            for rev in mine_data[data]:
              if rev != 'id' and 'status' in mine_data[data][rev]:
                if mine_data[data][rev]['status'] != 200:
                  row = [data, rev, mine_data[data][rev]['status']]
                  if 'type' in mine_data[data][rev]:
                    if 'type' not in head_row:
                      head_row.append('type')
                    row.append(mine_data[data][rev]['type'])
                  # Go through source items and pull out in-section items
                  if 'source' in mine_data[data][rev]:
                    source_list = []
                    for source in mine_data[data][rev]['source']:
                      source_list.append(source)
                    # match_list will be used to remove all URLs that contained a match.
                    match_list = []
                    for search in self.settings['search']:
                      # Add header row
                      if 'in ' + search not in head_row and 'not in ' + search not in head_row:
                        head_row.append('in ' + search)
                      in_source = []
                      for source in source_list:
                        if search in source:
                          in_source.append(source)
                          match_list.append(source)
                      row.append(in_source)
                    # Remove all matches from the course list to get the "not found" list
                    for match in match_list:
                      source_list.remove(match)
                    # Add header row for "Not in search list"
                    if 'not in searches' not in head_row:
                      head_row.append('not in searches')
                    row.append(source_list)

                  rows.append(row)
          writer.writerow(head_row)
          # Write to file
          for row in rows:
            writer.writerow(row)
