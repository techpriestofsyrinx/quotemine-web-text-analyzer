import os
import os.path
import time
import csv
import json

class Generic():
  pass

## LinkFilter: Filters discovered links
class DataImporter():
  parser_obj = Generic()
  miner_obj = Generic()
  settings = {}
  settings['import_filename'] = 'raw_export.dat'
  
  def import_data_process(self, **kwargs):
     if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      with open(os.path.abspath('.') + '/' + self.miner_obj.settings['report_path'] + self.settings['import_filename'] , 'r') as f:
        data = json.load(f)
        self.miner_obj.mine_data = data
        
