import re
import os
import os.path
import time
import csv
import json
from bs4 import BeautifulSoup

# This is meant to serve as an empty object to reference, as a default item.
class Generic():
  pass

## AuthorFilter: Read author from a page
#
# Add more top-level details here.
#
class AuthorFilter():
  # More often than not, you need to reference a parser or miner object.
  parser_obj = Generic()
  miner_obj = Generic()
  # Plugins often have settings. Extend the settings dict here.
  settings = {}
  settings['tag_select'] = 'span'
  settings['tag_regex'] = '[A-Z][a-z\,\.-]+'
  settings['exclude_list'] = ['By', 'And']
  # If your plugin has data, add it to a data dict.
  data = {}
  
  # OK, let's define the plugin functions. You may call your internal 
  # functions from these. Expected keyword arguments have also been added.

  ######################################################################
  ## Parser Plugins - Used with site_miner.process_url to parse responses made by each 
  # HTTP GET request to the URLs in the url_list.

  # Response data post-processor. Takes in the entire response object, which can either
  # be used, or the URL fed to a non-requests scraper like Ghost.py or Selenium
  def url_processor_postprocess(self, resp, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      html = resp.text
      soup = BeautifulSoup(html, 'html.parser')
      author = ''
      for tag in soup.select(self.settings['tag_select']):
        ret = ''
        match = re.findall(self.settings['tag_regex'], str(tag))
        for item in match:
          if item not in self.settings['exclude_list']:
            if not ret:
              ret = item
            else:
              ret = ret + ' ' + item
        author = author + ' ' + ret
      if resp.init_url in self.miner_obj.mine_data and self.miner_obj.revision_id in self.miner_obj.mine_data[resp.init_url]:
        self.miner_obj.mine_data[resp.init_url][self.miner_obj.revision_id]['author'] = author
      else:
        self.miner_obj.add_empty_url_data(resp.init_url)
        self.miner_obj.mine_data[resp.init_url][self.miner_obj.revision_id]['author'] = author
