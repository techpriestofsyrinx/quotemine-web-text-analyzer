import re
import os
import os.path
import time
import json
import csv

## Text Analyzer: Parses text inside HTML tags for word/language analysis
#
# Data:
#
# word_count - 
#
# Methods:
#
# parse_words - parses words from the Page Parser's handle_data output, and puts the words into the word_count dict
#
# calc_word_densities - converts the word_count dict into word densities, which in turn

class Generic():
  pass

class TextAnalyzer():
  parser_obj = Generic()
  miner_obj = Generic()
  data = {}
  data['simple_page_search'] = {}
  data['regex_tag_search'] = {}
  # Need to add this to the settings system later
  settings = {}
  settings['parser_report_types'] = ['total', 'pages']
  # Parser settings: Parse out words from the entire document or from select tags,
  # or parse out sentences from the entire document.
  settings['parsers'] = ['parse_words', 'parse_tags'] # , 'parse_sentences', 'regex_tag_search', 'simple_page_search'
  settings['parse_tags'] = ['title', 'h1']
  settings['parsers_add_data_to_main'] = True
  settings['min_length'] = 3
  settings['include'] = []
  settings['exclude'] = []
  # Simple searches: search pages and tags with Regular Expressions
  settings['simple_page_search'] = [('email', '\w+@\w+\.\w+')]
  settings['regex_tag_search_tags'] = ['title', 'h1']
  settings['regex_tag_search_terms'] = [('email', '\w+@\w+\.\w+')]
  # Import/Export settings
  settings['import_filename'] = 'textanalyzer_data.dat'
  settings['export_filename'] = 'textanalyzer_data.dat'

  # Simple page search - Do regex on the whole page and look for a search term
  def simple_page_search(self, resp, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      search_list = self.settings['simple_page_search']
      url = resp.init_url
      self.data['simple_page_search'][url] = {}
      self.data['simple_page_search'][url][self.miner_obj.revision_id] = {}
      for search in search_list:
        matches = re.findall(search[1], resp.text)
        self.data['simple_page_search'][url][search[0]] = matches
        if not url in self.miner_obj.mine_data:
          self.miner_obj.add_empty_url_data(url)
        elif self.miner_obj.revision_id not in self.miner_obj.mine_data[url]:
          self.miner_obj.add_empty_url_data(url)
        else:
          pass
        if search[0] not in self.miner_obj.mine_data[url][self.miner_obj.revision_id]:
          self.miner_obj.mine_data[url][self.miner_obj.revision_id][search[0]] = matches
        else:
          self.miner_obj.mine_data[url][self.miner_obj.revision_id][search[0]] += matches
  # Simple tag search - Do regex on the data in a tag and look for a search term
  def regex_tag_search(self, data, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if 'parser_obj' in kwargs:
        self.parser_obj = kwargs['parser_obj']
        search_list = self.settings['regex_tag_search_terms']
        url = self.parser_obj.active_url
        tag = self.parser_obj.active_tag
        if tag in self.settings['regex_tag_search_tags']:
          if not url + '-' + tag in self.data['regex_tag_search']:
            self.data['regex_tag_search'][url + '-' + tag] = {}
          if not self.miner_obj.revision_id in self.data['regex_tag_search'][url + '-' + tag]:
            self.data['regex_tag_search'][url + '-' + tag][self.miner_obj.revision_id] = {}
          for search in search_list:
            matches = re.findall(search[1], data)
            if not search[0] in self.data['regex_tag_search'][url + '-' + tag][self.miner_obj.revision_id]:
              self.data['regex_tag_search'][url + '-' + tag][self.miner_obj.revision_id][search[0]] = []
            for match in matches:
              self.data['regex_tag_search'][url + '-' + tag][self.miner_obj.revision_id][search[0]].append(match)
            if not url in self.miner_obj.mine_data:
              self.miner_obj.add_empty_url_data(url)
            if self.settings['parsers_add_data_to_main']:
              if search[0] + ' in ' + tag + ' tags' not in self.miner_obj.mine_data[url][self.miner_obj.revision_id]:
                self.miner_obj.mine_data[url][self.miner_obj.revision_id][search[0] + ' in ' + tag + ' tags'] = matches
              else:
                self.miner_obj.mine_data[url][self.miner_obj.revision_id][search[0] + ' in ' + tag + ' tags'] += matches

  # Get words - Creates a list of words discovered in a given string
  def parse_words(self, data, **kwargs):
    if 'parser_obj' in kwargs:
      self.parser_obj = kwargs['parser_obj']
      if 'word_count' not in self.data:
        self.data['word_count'] = {}
      tag = self.parser_obj.active_tag
      if tag != 'script':
        url = self.parser_obj.active_url
        if 'total' in self.settings['parser_report_types']:
          if 'scan_total' not in self.data['word_count']:
            self.data['word_count']['scan_total'] = {}
          if self.miner_obj.revision_id not in self.data['word_count']['scan_total']:
            self.data['word_count']['scan_total'][self.miner_obj.revision_id] = {'__total': 0}
        if 'pages' in self.settings['parser_report_types']:
          if url not in self.data['word_count']:
            self.data['word_count'][url] = {}
          if self.miner_obj.revision_id not in self.data['word_count'][url]:
            self.data['word_count'][url][self.miner_obj.revision_id] = {'__total': 0}
        # Get words from each string of data. Commented print statements are for checking parse logic
        '''print('-----')'''
        '''print(data)'''
        words = re.findall('\w*[A-Za-z]+\w*\S\w*[A-Za-z]+\w*|\w*[A-Za-z]+\w*', data)
        '''print(words)'''
        # Filter out newlines, which appear as individual 'n'
        for word in words:
          if word not in self.settings['exclude']:
            if word in self.settings['include'] or not (word == 'n' or len(word) < self.settings['min_length']):
              if 'pages' in self.settings['parser_report_types']:
                self.data['word_count'][url][self.miner_obj.revision_id]['__total'] += 1
                if word in self.data['word_count'][url]:
                  self.data['word_count'][url][self.miner_obj.revision_id][word] += 1
                else:
                  self.data['word_count'][url][self.miner_obj.revision_id][word] = 1
              if 'total' in self.settings['parser_report_types']:
                self.data['word_count']['scan_total'][self.miner_obj.revision_id]['__total'] += 1
                if word in self.data['word_count']['scan_total'][self.miner_obj.revision_id]:  
                  self.data['word_count']['scan_total'][self.miner_obj.revision_id][word] += 1
                else:
                  self.data['word_count']['scan_total'][self.miner_obj.revision_id][word] = 1
  
  # Parse tags - Extract all words from a selected list of tags
  def parse_tags(self, data, **kwargs):
    if 'parser_obj' in kwargs and 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      self.parser_obj = kwargs['parser_obj']
      tag = self.parser_obj.active_tag
      if tag in self.settings['parse_tags']:
        if 'tag_count_' + tag not in self.data:
          self.data['tag_count_' + tag] = {}
        words = re.findall('\w*[A-Za-z]+\w*\S\w*[A-Za-z]+\w*|\w*[A-Za-z]+\w*', data)
        url = self.parser_obj.active_url
        if words:
          if 'total' in self.settings['parser_report_types']:
            if 'scan_total' not in self.data['tag_count_' + tag]:
              self.data['tag_count_' + tag]['scan_total'] = {}
            if self.miner_obj.revision_id not in self.data['tag_count_' + tag]['scan_total']:
              self.data['tag_count_' + tag]['scan_total'][self.miner_obj.revision_id] = {'__total': 0}
          if 'pages' in self.settings['parser_report_types']:
            if url not in self.data['tag_count_' + tag]:
              self.data['tag_count_' + tag][url] = {}
            if self.miner_obj.revision_id not in self.data['tag_count_' + tag][url]:
              self.data['tag_count_' + tag][url][self.miner_obj.revision_id] = {'__total': 0, '__full': [data]}
              if self.settings['parsers_add_data_to_main']:
                self.miner_obj.mine_data[url][self.miner_obj.revision_id][tag] = data
            if '__full' in self.data['tag_count_' + tag][url]:
              self.data['tag_count_' + tag][url]['__full'].append(data)
            else:
              self.data['tag_count_' + tag][url]['__full'] = [data]
          for word in words:
            if word in self.settings['include'] or not (word == 'n' or len(word) <= self.settings['min_length']):
              if 'total' in self.settings['parser_report_types']:
                self.data['tag_count_' + tag]['scan_total'][self.miner_obj.revision_id]['__total'] += 1
                if word in self.data['tag_count_' + tag]['scan_total'][self.miner_obj.revision_id]:  
                  self.data['tag_count_' + tag]['scan_total'][self.miner_obj.revision_id][word] += 1
                else:
                  self.data['tag_count_' + tag]['scan_total'][self.miner_obj.revision_id][word] = 1
              if 'pages' in self.settings['parser_report_types']:
                self.data['tag_count_' + tag][url][self.miner_obj.revision_id]['__total'] += 1
                if word in self.data['tag_count_' + tag][url][self.miner_obj.revision_id]:
                  self.data['tag_count_' + tag][url][self.miner_obj.revision_id][word] += 1
                else:
                  self.data['tag_count_' + tag][url][self.miner_obj.revision_id][word] = 1

  # Calculate word densities
  def calc_word_densities(self):
    calc_list = [('word_count', 'word_density_total')]
    for tag in self.settings['parse_tags']:
      calc_list.append(('tag_count_' + tag, 'word_density_' + tag))
    for item in calc_list:
      if item[0] not in self.data:
        self.data[item[0]] = {}
      if item[1] not in self.data:
        self.data[item[1]] = {}
      for url in self.data[item[0]]:
        for rev in self.data[item[0]][url]:
          if not url in self.data[item[1]]:
            self.data[item[1]][url] = {}
          if not rev in self.data[item[1]][url]:
            self.data[item[1]][url][rev] = {}
            data = self.data[item[0]][url][rev]
            for word in data:
              if word != '__full':
                if data['__total'] != 0:
                  self.data[item[1]][url][rev][word] = data[word] / data['__total']
                else:
                  self.data[item[1]][url][rev][word] = 0

  ######################################################################
  # Plugin callbacks
  ######################################################################

  # URL Processor handle_data callback
  def url_processor_data(self, data, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if 'parser_obj' in kwargs:
        self.parser_obj = kwargs['parser_obj']
        if 'parse_words' in self.settings['parsers']:
          self.parse_words(data, miner_obj=self.miner_obj, parser_obj = self.parser_obj)
        if 'parse_sentences' in self.settings['parsers']:
          self.parse_sentences(data, miner_obj=self.miner_obj, parser_obj = self.parser_obj)
        if 'parse_tags' in self.settings['parsers']:
          self.parse_tags(data, miner_obj=self.miner_obj, parser_obj = self.parser_obj)
        if 'regex_tag_search' in self.settings['parsers']:
          self.regex_tag_search(data, miner_obj=self.miner_obj, parser_obj = self.parser_obj)

  # URL Processor post-process: search entire response object + previously found data
  def url_processor_postprocess(self, resp, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if 'simple_page_search' in self.settings['parsers']:
        self.simple_page_search(resp, miner_obj=self.miner_obj)

  # Periodic Update: Export Data and generate reports
  def mine_periodic_update(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if self in self.miner_obj.plugin_list['report']['process']:
        self.report_data_process(miner_obj=self.miner_obj)
      if self in self.miner_obj.plugin_list['export']['process']:
        self.export_data_process(miner_obj=self.miner_obj)

  # Process Data: calculate word densities
  def process_data(self, **kwargs):
    self.calc_word_densities()

  # Generate text analysis reports
  def report_data_process(self, **kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      page_output = {}
      # Export all data as a separate report per collection of data.
      for collection in self.data:
        page_output[collection] = {}
        # Insert 
        for data in self.data[collection]:
          if data == 'scan_total':
            if self.data[collection][data]:
              with open(os.path.abspath('.') + '/' + self.miner_obj.settings['report_path'] + collection + '-total.csv', 'w', newline='') as f:
                writer = csv.writer(f)
                for rev in self.data[collection][data]:
                  for word in self.data[collection][data][rev]:
                    try:
                      writer.writerow([rev, word, self.data[collection][data][rev][word]])
                    except:
                      pass
          else:
            page_output[collection][data] = self.data[collection][data]
            
        for coll in page_output:
          if page_output[coll]:
            with open(os.path.abspath('.') + '/' + self.miner_obj.settings['report_path'] + coll + '-pages.csv', 'w', newline='') as f:
              writer = csv.writer(f)
              for data in page_output[coll]:
                for rev in page_output[coll][data]:
                  for word in page_output[coll][data][rev]:
                    try:
                      writer.writerow([data, rev, word, page_output[coll][data][rev][word]])
                    except:
                      pass
    else:
      print('TextAnlayzer: No data was given')
  
  # Import Data
  def import_data_process(self,**kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      with open(os.path.abspath('.') + '/' + self.miner_obj.settings['report_path'] + self.settings['import_filename'] , 'r') as f:
        data = json.load(f)
        self.data = data

  # Export Data
  def export_data_process(self,**kwargs):
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      data = self.data
      with open(os.path.abspath('.') + '/' + self.miner_obj.settings['report_path'] + self.settings['export_filename'] , 'w') as f:
        json.dump(data, f)
