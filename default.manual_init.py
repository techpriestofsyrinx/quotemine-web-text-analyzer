# Miner
from miners.site_miner import SiteMiner
# Parser
from parsers.pageparser import PageParser
# Plugins
from plugins.authorfilter import AuthorFilter
from plugins.brokenlinkdata import BrokenLinkData
from plugins.datacleanup import DataCleanup
from plugins.dataexporter import DataExporter
from plugins.dataimporter import DataImporter
from plugins.datefilter import DateFilter
from plugins.linkfilter import LinkFilter
from plugins.sentimentanalyzer import SentimentAnalyzer
from plugins.sitemapper import SiteMapper
from plugins.textanalyzer import TextAnalyzer
from plugins.urllistbuilder import URLListBuilder
# Other Imports
import time

miner = SiteMiner()

########################################################################
# miner.settings
########################################################################

# Mining settings
miner.settings['max_items'] = {}
miner.settings['max_items']['total'] = 10
miner.settings['max_items']['internal'] = 10
miner.settings['max_items']['external'] = 1
miner.settings['max_items']['file'] = 1
miner.settings['mine_internal'] = True
miner.settings['mine_file'] = False
miner.settings['mine_external'] = False
miner.settings['periodic_update_interval'] = 10
miner.settings['scan_error_max'] = 10

# URL Processor settings
miner.settings['parser_debug_mode'] = False
miner.settings['headers'] = {'user-agent': 'QuoteMine Web Text Analyzer/0.0.1'}
miner.settings['kw'] = {'stream': True, 'timeout': 10}
miner.settings['domain'] = 'http://www.example.com'
miner.settings['url_list'] = {'internal':['http://www.example.com'], 'external': [], 'file': []}
miner.settings['time_between_requests'] = 3

# Report settings
# The trailing slash must be present, and the folder must be create ahead of time
miner.settings['report_path'] = 'reports/'

# Current Revision
# This lists the current time and date. More format options at: https://docs.python.org/2/library/time.html#time.strftime
miner.revision_id = time.strftime('%H:%M:%S-%d-%m-%Y',time.localtime())

########################################################################
# plugin.settings
########################################################################

# Load parser
page_parser = PageParser()
# Load plugins
author_filter = AuthorFilter()
broken_link_data = BrokenLinkData()
data_cleanup = DataCleanup()
data_exporter = DataExporter()
data_importer = DataImporter()
date_filter = DateFilter()
link_filter = LinkFilter()
sentiment_analyzer = SentimentAnalyzer()
site_mapper = SiteMapper()
text_analyzer = TextAnalyzer()
url_list_builder = URLListBuilder()

#### Plugin settings for AuthorFilter
# tag_select: CSS selector for finding tags that contain the author string
# tag_regex: RegEx which will be carried out on found tags/data
# exclude_list: Exclude these words (exact match)
author_filter.settings['tag_select'] = 'span'
author_filter.settings['tag_regex'] = '[A-Z][a-z\,\.-]+'
author_filter.settings['exclude_list'] = ['By', 'And']

#### Plugin settings for BrokenLinkData
# Search through URL sources, to see if any of the substrings exist
broken_link_data.settings['search'] = []

#### Plugin settings for DataCleanup
# 'remove' - Deletes any URL that wasn't scanned
# 'mark' - Marks Scanned and Unscanned URLs as such
data_cleanup.settings['clean_method'] = 'remove' # 'remove' or 'mark'

#### Plugin settings for DataExporter
# export_methods: write a CSV file (report of all data) and/or JSON file (save file)
data_exporter.settings['export_methods'] = ['csv_data', 'json_data']
data_exporter.settings['json_export_filename'] = 'raw_export.dat'
data_exporter.settings['csv_export_filename'] = 'url_data.csv'

#### Plugin settings for DataImporter
# Normally, the import path is the same as the export path's save file
data_importer.settings['import_filename'] = 'raw_export.dat'

#### Plugin settings for DateFilter
# Select methods:
# url - Get date from URL
# select - Use a CSS selector, or just the name of tag you want to use
date_filter.settings['method'] = 'select' # Use only one of 'url' or 'select'
# Search settings; go for day/month/year components separately, OR process the whole date
date_filter.settings['date_search'] = {}
date_filter.settings['date_search']['type'] = 'entire_date' # Use either 'entire_date' for 12/03/2015 type dates, or 'partial_date' for extracting Dec 03, 2015 type dates by parts
date_filter.settings['date_search']['day_regex'] = '(?<!\d)(\d\d|\d)(?!\d)'
date_filter.settings['date_search']['month_dict'] = {'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04', 'May': '05', 'Jun': '06', 'Jul': '07', 'Aug': '08', 'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12'}
date_filter.settings['date_search']['year_regex'] = '(?<!\d)\d\d\d\d(?!\d)'
date_filter.settings['date_search']['entire_date_regex'] = '(\d\d\d\d|\d\d)-(\d\d|\d)-(\d\d|\d)' # '(\d\d\d\d|\d\d)\/(\d\d|\d)\/(\d\d|\d)'
# URL: Get the date out of the URL
date_filter.settings['url'] = {}
date_filter.settings['url']['require'] = []
date_filter.settings['url']['exclude'] = []
date_filter.settings['url']['allow_subdomains'] = (False, 'example.com')
# Tag attribute: get a tag, identify the tag with a certain attribute value, and use data in a specific data attribute
date_filter.settings['select'] = 'time'

#### Plugin settings for LinkFilter
## Settings for including or excluding URLs
# require: URLs must contain a substring or set of substrings. All others are excluded.
# - Base list is OR basis, you can add a list to the list to use AND basis
# - example: [['section', '2016'], ['section', '2015'], 'category'] translates to:
#   "If the URL contains 'section' AND '2016' , OR 'section' AND '2016', OR 'category', include it. Otherwise, exclude.
# exclude: Exclude the URL if it contains any of the listed substrings
# include: Include the item if it contains any of the listed substrings, even if it meets exclusion criteria
# include_override: If any of these substrings are matched, cancel inclusion
# Note: 'required' overrides include/exclude logic
link_filter.settings['require'] = []
link_filter.settings['exclude'] = []
link_filter.settings['include'] = []
link_filter.settings['include_override'] = []
# prioritize: URLs are usually added to the bottom of the scan list. 
# If it has any of the listed substrings, put it at the top of the list to be scanned sooner.
link_filter.settings['prioritize'] = []
# TODO verify subdomains work
link_filter.settings['allow_subdomains'] = (False, 'example.com')
# Site seciton based on URL component
# For http://example.com/this/that, "2" gets 'example.com', "3" gets 'this', "4" gets 'that' , etc.
link_filter.settings['section_position'] = 3
# d3_chart: Generate D3 data for a site URL structure chart: http://bl.ocks.org/mbostock/4339083
link_filter.settings['report_methods'] = ['d3_chart']
## Limit found links to a maximum depth.
# "Depth" is based on the number of clicks away from the URLs given at the beginning.
# Depending on the site, 0/1 is the home page, 1/2 is all links on the home page, etc.
# Do a test run to see whether the scanner will start form 0 or 1, 
# or set to False if you don't want to limit on depth.
link_filter.settings['max_depth'] = False

#### Plugin settings for SentimentAnalyzer
# api_call_criteria: Call AlchemyLanguage API if URL or page text 
# matches one of the listed substrings
sentiment_analyzer.settings['api_call_criteria'] = {}
sentiment_analyzer.settings['api_call_criteria']['url'] = []
sentiment_analyzer.settings['api_call_criteria']['text'] = []
# API endpoint settings
sentiment_analyzer.settings['api_endpoint'] = 'http://gateway-a.watsonplatform.net/calls/url/URLGetRankedNamedEntities'
sentiment_analyzer.settings['api_key'] = 'your_key'
sentiment_analyzer.settings['api_args'] = [('sentiment', '1'),('outputMode', 'json')]
## Entity settings
# add_to_mine_data: If an entity name exactly matches an item in the list, add it to the miner's URL data
# sentiment_data: which types of sentiment data to add
# require: Only include these entities
# exclude: Do not include these entities
sentiment_analyzer.settings['entities'] = {}
sentiment_analyzer.settings['entities']['add_to_mine_data'] = []
sentiment_analyzer.settings['entities']['sentiment_data'] = ['type', 'score']
sentiment_analyzer.settings['entities']['require'] = []
sentiment_analyzer.settings['entities']['exclude'] = []
# Import/Export settings
sentiment_analyzer.settings['report_filename'] = 'sentiment_report.csv'
sentiment_analyzer.settings['import_filename'] = 'sentiment_data.dat'
sentiment_analyzer.settings['export_filename'] = 'sentiment_data.dat'

#### Plugin settings for SiteMapper
# end_page: do not include as part of network map
# generate_methods: generate_d3_tree creates d3 data for a netowrk map: http://bl.ocks.org/mbostock/4062045
# revision: Select a revision to map
site_mapper.settings['end_page'] = 'index.html'
site_mapper.settings['generate_methods'] = ['generate_d3_tree']
site_mapper.settings['revision'] = ''
#import/export save file
site_mapper.settings['import_filename'] = 'sitemapper.json'
site_mapper.settings['export_filename'] = 'sitemapper.json'

#### Plugin settings for TextAnalyzer
# total: Word count for entire scan
# pages: Word counts per URL
text_analyzer.settings['parser_report_types'] = ['total', 'pages']
## Parser settings: Parse out words from the entire document or from select tags.
# Parsers:
# - parse_words: Harvest all words from a page
# - parse_tags: Harvest all words from a specific set of tags
# - regex_tag_search: Search select tags with regex
# - simple_page_search: Search the entire page with regex
# parse_tags: List tags to search for parse_tags. 
# regex_tag_search_tags: List tags to search for regex_tag_search. 
# 'title' is the page's meta title, and a well-designed page uses h1 as a visible title
# parsers_add_data_to_main: Add found data to the miner's URL data structure
text_analyzer.settings['parsers'] = ['parse_words', 'parse_tags'] # , 'regex_tag_search', 'simple_page_search'
text_analyzer.settings['parse_tags'] = ['title', 'h1']
text_analyzer.settings['regex_tag_search_tags'] = ['title', 'h1']
text_analyzer.settings['parsers_add_data_to_main'] = True
# min_length: Minimum length of words to include
# include: Include even if the word is too short
# exclude: Exclude even if the word is long enough
text_analyzer.settings['min_length'] = 3
text_analyzer.settings['include'] = []
text_analyzer.settings['exclude'] = []
# Regex searches: search pages and tags with Regular Expressions
# Format: List of tuples containing a label and the regular expression:
# Example of 2 searches: [('label1', 'regex1'), ('label2', 'regex2')]
# simple_page_search: Regex searches for simple_page_search parser
# regex_tag_search_terms: Regex searches for regex_tag_search parser
text_analyzer.settings['simple_page_search'] = [('email', '\w+@\w+\.\w+')]
text_analyzer.settings['regex_tag_search_terms'] = [('email', '\w+@\w+\.\w+')]
# Import/Export settings
text_analyzer.settings['import_path'] = miner.settings['report_path'] + 'text_data.dat'
text_analyzer.settings['export_path'] = miner.settings['report_path'] + 'text_data.dat'

#### Plugin settings for URLListBuilder
# Choose a list builder type. url_data_builder usualy pairs with data_importer to load links from save files
url_list_builder.settings['list_builders'] = ['numeric_builder', 'url_data_builder', 'url_depth_builder']
## Numeric Builder
# This builder is used when you need to generate a lot of links of the 
# type http://example.com?page=1, http://example.com?page=2, etc.
# base_url and end_url: The beginning and end of the URL, with the number going in between
# category: URL category for the miner: can be 'internal', 'external', or 'file'
# range_start and range_stop: the start and stop of the number range
url_list_builder.settings['numeric_builder'] = {}
url_list_builder.settings['numeric_builder']['base_url'] = 'http://www.example.com?param='
url_list_builder.settings['numeric_builder']['end_url'] = '&param2=whatever'
url_list_builder.settings['numeric_builder']['category'] = 'internal'
url_list_builder.settings['numeric_builder']['range_start'] = 1
url_list_builder.settings['numeric_builder']['range_stop'] = 2
## URL Data Builder
# This goes through the URL data, and will include/exclude URLs with certain types of data
# - Requires data to be present. Usually this is paired with DataImporter
# method: you can 'include' or 'exclude' URLs, but not both in this module
# data_type: the type of data associated with the URL. Look at a URL data 
# report from DataExporter to see what's available
# include/exclude: Include or exclude URLs with matching data.
url_list_builder.settings['url_data_builder'] = {}
url_list_builder.settings['url_data_builder']['method'] = 'exclude'
url_list_builder.settings['url_data_builder']['data_type'] = 'status'
url_list_builder.settings['url_data_builder']['include'] = ['Scan Error', '404']
url_list_builder.settings['url_data_builder']['exclude'] = ['200']
## URL Depth Builder
# This goes through the URL data, and includes URLs based on depth and length
# depth_label: in the URL data, the name of the depth property. Usually 'depth'
# max_depth: only include URLs of this depth or less
# max_section_length: Roughly speaking, the number of slashes (/) in the URL
# exclude: Exclude URLs matching a substring
url_list_builder.settings['url_depth_builder'] = {}
url_list_builder.settings['url_depth_builder']['depth_label'] = 'depth'
url_list_builder.settings['url_depth_builder']['max_depth'] = 1
url_list_builder.settings['url_depth_builder']['max_section_length'] = 5
url_list_builder.settings['url_depth_builder']['exclude'] = ''

########################################################################
# Place plugins into miner callbacks
# 
# By default, everything is 'on'
########################################################################

#### Miner plugins - called for the entire mining process
miner.plugin_list['mine'] = {'preprocess': [],'postprocess': [], 'periodic_update':[]}

###### Miner pre-processing
miner.plugin_list['mine']['preprocess'] = [url_list_builder]

###### Miner periodic updates/saves
miner.plugin_list['mine']['periodic_update'] = [broken_link_data, data_exporter, link_filter, sentiment_analyzer, site_mapper, text_analyzer]

###### Miner post-processing
miner.plugin_list['mine']['postprocess'] = [data_cleanup]

#### URL Processor Plugins - called per URL mined
miner.plugin_list['url_processor'] = {'parse': [],'tag': [],'data': [],'postprocess': []}

###### URL Processor - parser
miner.plugin_list['url_processor']['parse'] = [page_parser]

###### URL Processor - handle_data callbacks
miner.plugin_list['url_processor']['data'] = [text_analyzer]

###### URL Processor - handle_tag callbacks
miner.plugin_list['url_processor']['tag'] = [date_filter, link_filter]

###### URL Processor - Process the response object post
miner.plugin_list['url_processor']['postprocess'] = [author_filter, date_filter, sentiment_analyzer, text_analyzer]

#### Data Processor Plugins
miner.plugin_list['process_data'] = {}
miner.plugin_list['process_data']['process'] = [site_mapper, text_analyzer]

#### Reporting Plugins - Generate Reports
miner.plugin_list['report'] = {}
miner.plugin_list['report']['preprocess'] = []
miner.plugin_list['report']['process'] = [broken_link_data, link_filter, sentiment_analyzer, text_analyzer]
miner.plugin_list['report']['postprocess'] = []

#### Import/Export plugins
###### Import Plugins - Import Data
miner.plugin_list['import'] = {}
miner.plugin_list['import']['process'] = [data_importer, link_filter, sentiment_analyzer, site_mapper, text_analyzer]
miner.plugin_list['import']['postprocess'] = [url_list_builder]

###### Export Plugins - Export Data
miner.plugin_list['export'] = {}
miner.plugin_list['export']['preprocess'] = []
miner.plugin_list['export']['process'] = [data_exporter, link_filter, sentiment_analyzer, site_mapper, text_analyzer]
miner.plugin_list['export']['postprocess'] = []


########################################################################
# Execute the miner!
########################################################################

# Importer Example - Comment "Run the mine" and uncomment this to pull data from an old report
#miner.import_data()

miner.programmatic_run(import=False, mine=True, process=True, report=True, export=True
