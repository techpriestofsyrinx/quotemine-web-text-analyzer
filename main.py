# import libraries
from miners import site_miner

# Kivy imports
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.lang import Builder

from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout


# Quotemine settings

#Builder.load_file('settings.kv')

# QuoteMine app code

class QuoteMine(Widget):
  
  def update(self, dt):
    print('hihi')



# Quotemine startup code 

class QuoteMineApp(App):
  def build(self):
    qm = QuoteMine()
    Clock.schedule_interval(qm.update, 1)
    return qm

if __name__ == '__main__':
  QuoteMineApp().run()
