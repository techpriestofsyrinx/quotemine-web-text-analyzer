from html.parser import HTMLParser

class Generic():
  pass

## Page Parser: Parses HTML pages
class PageParser(HTMLParser):
  data_parser_list = {}
  tag_parser_list = {}
  miner_obj = Generic()
  active_tag = ''
  active_attrs = []
  active_url = ''

  def url_processor_parse(self, data, **kwargs):
    if 'tag_parser_list' in kwargs:
      self.tag_parser_list = kwargs['tag_parser_list']
    if 'data_parser_list' in kwargs:
      self.data_parser_list = kwargs['data_parser_list']
    if 'miner_obj' in kwargs:
      self.miner_obj = kwargs['miner_obj']
      if 'active_url' in kwargs:
        self.active_url = kwargs['active_url']
        self.feed(data)

  def handle_starttag(self, tag, attrs):
    self.active_tag = tag
    self.active_attrs = attrs
    if self.tag_parser_list:
      for plugin in self.tag_parser_list:
        plugin(tag, attrs, parser_obj=self, miner_obj=self.miner_obj)

  def handle_data(self, data):
    if self.data_parser_list:
      for plugin in self.data_parser_list:
        plugin(data, parser_obj=self, miner_obj=self.miner_obj)

  def handle_endtag(self, tag):
    pass
